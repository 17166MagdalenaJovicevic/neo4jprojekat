using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Neo4jClient;
using Neo4jServer.Models;

namespace Neo4jServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PesmaController : ControllerBase
    {
        private readonly IGraphClient _client;

        public PesmaController(IGraphClient client)
        {
            _client = client;
        }

        private async Task<bool> Token(string Username, string authorization) {

            await _client.ConnectAsync();
            var token  = await _client.Cypher.Match("(t:Token {Username:$username})").WithParam("username",Username).Return((t) => t.As<Token>()).ResultsAsync;
            bool fleg = false;

            var dict = new Dictionary<string, Object>();
            dict.Add("username",Username);
            
            foreach(var t in token) {
                dict.Add("tokenString",t.TokenString);

                if (t.TokenString == authorization) {
                    if (new DateTime(t.VremeKreiranja).AddHours(6).Ticks < DateTime.UtcNow.Ticks) {
                        await _client.Cypher.Match("(t:Token {Username:$username,TokenString:$tokenString})").WithParams(dict).DetachDelete("t").ExecuteWithoutResultsAsync();
                    }
                    else {
                        fleg = true;
                        break;
                    }    
                }
                else {
                    if (new DateTime(t.VremeKreiranja).AddHours(6).Ticks < DateTime.UtcNow.Ticks) {
                        await _client.Cypher.Match("(t:Token {Username:$username,TokenString:$tokenString})").WithParams(dict).DetachDelete("t").ExecuteWithoutResultsAsync();
                    }
                }

                dict.Remove("tokenString");
            }
            return fleg;

        }

        [HttpPost]
        [Route("DodajNovuPesmu/{Username}")]
        public async Task<string> DodajNovuPesmu([FromBody] Pesma pesma,[FromRoute]string Username) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return "Los token";
            }
            await _client.ConnectAsync();

            var pesmica = await _client.Cypher.Match("(p:Pesma{Id: $id})").WithParam("id",pesma.ID()).Return(p=>p.As<Pesma>()).ResultsAsync;
            if(pesmica.FirstOrDefault() != default)
            {
                return "Pesma vec postoji";
            }

            if (pesma.NazivPesme == "" ) {
                return "Nedostaje naziv";
            }

            if (pesma.TekstPesme == "" ) {
                return "Nedostaje tekst pesme";
            }

            if (pesma.Autor == "" ) {
                return "Nedostaje ime autora pesme";
            }

            var dict = new Dictionary<string, Object>();
            dict.Add("id",pesma.ID());
            dict.Add("nazivPesme",pesma.NazivPesme);
            dict.Add("zbirkaPesama",null);
            if (pesma.ZbirkaPesama != "") {
                dict["zbirkaPesama"] = pesma.ZbirkaPesama;
            }
            dict.Add("godinaObjavljivanja",pesma.GodinaObjavljivanja);
            dict.Add("tekstPesme",pesma.TekstPesme);
            dict.Add("autor",pesma.Autor);
            


                await _client.Cypher.Create("(p:Pesma {Id: $id, NazivPesme: $nazivPesme, ZbirkaPesama: $zbirkaPesama,"+
                " GodinaObjavljivanja: $godinaObjavljivanja, TekstPesme: $tekstPesme, Autor: $autor, Popularnost: 0})").WithParams(dict).ExecuteWithoutResultsAsync();

            

            var pesnik = await _client.Cypher.Match("(p:Pesnik {Id:$id})").WithParam("id",Pesnik.ID(pesma.Autor)).Return(p=> p.As<Pesnik>()).ResultsAsync;
            if(pesnik.FirstOrDefault() != default)
            {
                await _client.Cypher.Match("(p:Pesnik {Id:\'"+Pesnik.ID(pesma.Autor)+"\'})").Match("(m:Pesma {Id:\'"+pesma.ID()+"\'})")
                .Create("(p)-[:AUTOR]->(m)").ExecuteWithoutResultsAsync();

            }
            

            if (pesma.Epoha.NazivEpohe != "") {
                var epoha = _client.Cypher.Match("(e:Epoha {NazivEpohe:$nazivEpohe})").WithParam("nazivEpohe",pesma.Epoha.NazivEpohe);
                var rezultat = await epoha.Return((e) =>e.As<Epoha>()).ResultsAsync;
                if (rezultat.FirstOrDefault() != default) {    
                    await epoha.Match("(m:Pesma {Id:$id})").WithParam("id",pesma.ID()).Create("(e)<-[:EPOHA]-(m)").ExecuteWithoutResultsAsync();
                }
                else {
                    _client.Cypher.Create("(e:Epoha {NazivEpohe: \'" + pesma.Epoha.NazivEpohe + "\', TrajanjeEpohe: \'" + 
                    pesma.Epoha.TrajanjeEpohe + "\'})").ExecuteWithoutResultsAsync().Wait();
                    await _client.Cypher.Match("(m:Pesma {Id:$id})").WithParam("id",pesma.ID()).Match("(e:Epoha {NazivEpohe:$nazivEpohe})").WithParam("nazivEpohe",pesma.Epoha.NazivEpohe).Create("(e)<-[:EPOHA]-(m)").ExecuteWithoutResultsAsync();
                }
            }

            if (pesma.Teme != null && pesma.Teme.Count > 0) {
                foreach(var t in pesma.Teme) {
                    if (t.NazivTeme != "") {
                        var tema = _client.Cypher.Match("(t:Tema {NazivTeme:$nazivTeme})").WithParam("nazivTeme",t.NazivTeme);
                        var rezultat = await tema.Return((t) => t.As<Tema>()).ResultsAsync;
                        if (rezultat.FirstOrDefault() != default) {
                            await tema.Match("(m:Pesma {Id:$id})").WithParam("id",pesma.ID()).Merge("(t)-[:TEMA]->(m)").ExecuteWithoutResultsAsync();
                        }
                        else {
                            _client.Cypher.Create("(t:Tema {NazivTeme: \'" +t.NazivTeme + "\'})").ExecuteWithoutResultsAsync().Wait();
                            
                            await tema.Match("(m:Pesma {Id:$id})").WithParam("id",pesma.ID()).Merge("(t)-[:TEMA]->(m)").ExecuteWithoutResultsAsync();
                        }
                    }
                    
                }
            }
            return "Pesma uspesno dodata!";

        }

        [HttpGet]
        [Route("VratiPesmu/{NazivPesme}/{Autor}")]
        public async Task<Pesma> VratiPesmu([FromRoute] string Autor, [FromRoute] string NazivPesme) {

            await _client.ConnectAsync();
            var pesma = await _client.Cypher.OptionalMatch("(p:Pesma{Id:\'"+Pesma.ID(Autor,NazivPesme)+"\'})").Return((p) => p.As<Pesma>()).ResultsAsync;
            if (pesma.FirstOrDefault() == default) {
                return pesma.First();
            }

            var epoha = await _client.Cypher.Match("(p:Pesma{Id:\'"+Pesma.ID(Autor,NazivPesme)+"\'})-[:EPOHA]->(e:Epoha)").Return((e) => e.As<Epoha>()).ResultsAsync;
            var pesmaObj = pesma.First();
            pesmaObj.Epoha = epoha.FirstOrDefault();

            var teme = await _client.Cypher.Match("(p:Pesma{Id:\'"+Pesma.ID(Autor,NazivPesme)+"\'})<-[:TEMA]-(t:Tema)").Return((t) => t.As<Tema>()).ResultsAsync;
            pesmaObj.Teme = new List<Tema>();
            foreach(var t in teme) {
                if (teme != null) {
                    pesmaObj.Teme.Add(t);
                }
            }

            // Povecava se popularnost
            await _client.Cypher.Match("(p:Pesma{Id:\'"+Pesma.ID(Autor,NazivPesme)+"\'})").Set("p.Popularnost = p.Popularnost + 1").Return(p => new {pesma = p.As<Pesma>()}).ResultsAsync;

            return pesmaObj;
            
        }

        [HttpGet]
        [Route("VratiPesmePoPopularnosti")]
        public async Task<List<Pesma>> VratiPesmePoPopularnosti() {
            await _client.ConnectAsync();
            var pesma = await _client.Cypher.OptionalMatch("(p:Pesma)").Return((p) => /*new {pesma =*/ p.As<Pesma>()).OrderByDescending("p.Popularnost").Limit(10).ResultsAsync;
            var lista = new List<Pesma>();
            foreach(var p in pesma) {
                lista.Add(p);
            }
            return lista;
        }

        [HttpGet]
        [Route("PretraziPesme/{tekstPretrage}")]
        public async Task<List<Pesma>> PretraziPesme([FromRoute] string tekstPretrage) {
            await _client.ConnectAsync();

            string[] reciPretrage = tekstPretrage.Split(' ');
            var lista = new List<Pesma>();
            foreach(var rec in reciPretrage) {
                var pesme = await _client.Cypher.Match("(p:Pesma)").Where("p.NazivPesme =~ \'(?i).*"+rec+".*\'").Return((p) => p.As<Pesma>()).Limit(20).ResultsAsync;
                
                foreach(var p in pesme) {
                    lista.Add(p);
                }
            }
            return lista;
        }

        [HttpGet]
        [Route("VratiPesmeIzIsteZbirke/{imeZbirke}/{NazivPesme}/{Autor}")]
        public async Task<List<Pesma>> VratiPesmeIzIsteZbirke([FromRoute] string imeZbirke, [FromRoute] string NazivPesme, [FromRoute] string Autor) {
            await _client.ConnectAsync();

            

            var pesme = await _client.Cypher.Match("(m:Pesma {Id:\'"+Pesma.ID(Autor,NazivPesme)+"\'})<-[:AUTOR]-(t:Pesnik)-[:AUTOR]->(p:Pesma{ZbirkaPesama:\'"+imeZbirke+"\'})").Return((p) => p.As<Pesma>()).ResultsAsync;
            var lista = new List<Pesma>();    
            foreach(var p in pesme) {
                lista.Add(p);
            }
            return lista;

        }

        [HttpGet]
        [Route("VratiSvePesme/{Username}")]
        public async Task<List<Pesma>> VratiSvePesme([FromRoute] string Username) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);
            
            var lista = new List<Pesma>();    
            if (fleg == false) {
                return lista;
            }

            await _client.ConnectAsync();

            var pesme = await _client.Cypher.Match("(p:Pesma)").Return((p) => p.As<Pesma>()).ResultsAsync;
            
            foreach(var p in pesme) {
                lista.Add(p);
            }
            return lista;

        }

        [HttpGet]
        [Route("VratiPesmePoTemamaPesmePopularnosti/{NazivPesme}/{Autor}")]
        public async Task<List<Pesma>> VratiPesmePoTemamaPesmePopularnosti([FromRoute] string Autor, [FromRoute] string NazivPesme) {

            await _client.ConnectAsync();
            var pesma = await _client.Cypher.Match("(x:Pesma {Id:\'"+Pesma.ID(Autor, NazivPesme)+"\'})<-[:TEMA]-(t:Tema)-[:TEMA]->(p:Pesma)").Return((p) => p.As<Pesma>()).OrderByDescending("p.Popularnost").Limit(10).ResultsAsync;
            var lista = new List<Pesma>();
            foreach(var p in pesma) {
                lista.Add(p);
            }
            return lista;

        }

        [HttpGet]
        [Route("VratiPesmeIstogPesnika/{NazivPesme}/{Autor}")]
        public async Task<List<Pesma>> VratiPesmeIstogPesnika([FromRoute] string Autor, [FromRoute] string NazivPesme) {
            await _client.ConnectAsync();
            var pesma = await _client.Cypher.Match("(x:Pesma {Id:\'"+Pesma.ID(Autor, NazivPesme)+"\'})<-[:AUTOR]-(t:Pesnik)-[:AUTOR]->(p:Pesma)").Return((p) => p.As<Pesma>()).OrderByDescending("p.Popularnost").Limit(10).ResultsAsync;
            var lista = new List<Pesma>();
            foreach(var p in pesma) {
                lista.Add(p);
            }
            return lista;
        }

        

        [HttpPut]
        [Route("IzmeniPesmu/{AutorPesme}/{NazivPesme}/{Username}")] //puca u 359. liniji. Neki null pointer se javlja.

        public async Task<string> IzmeniPesmu([FromRoute]string AutorPesme,[FromRoute]string NazivPesme,[FromBody]Pesma pesma,[FromRoute]string Username){

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return "Los token";
            }
            await _client.ConnectAsync();

            if (pesma.NazivPesme == "" ) {
                return "Nedostaje naziv";
            }

            if (pesma.Autor == "") {
                return "Nedostaje autor";
            }


            var pesmica = await _client.Cypher.Match("(p:Pesma{Id: \'" + Pesma.ID(AutorPesme,NazivPesme) + "\'})").Return(p=>p.As<Pesma>()).ResultsAsync;
            var pesmaKojaSeMenja = pesmica.FirstOrDefault(); //pesmaKojaSeMenja

            if(pesmaKojaSeMenja == default)
            {
                return "Pesma ne postoji";
            }

            var pesmaa = _client.Cypher.Match("(p:Pesma{Id: \'" + pesmaKojaSeMenja.ID() + "\'})"); //novaPesma

            /*if(pesma.NazivPesme != pesmaKojaSeMenja.NazivPesme){

                await pesmaa.Set("p.NazivPesme = \'"+ pesma.NazivPesme + "\'").ExecuteWithoutResultsAsync();
            }*/

             if(pesma.ZbirkaPesama != "" && pesma.ZbirkaPesama != pesmaKojaSeMenja.ZbirkaPesama){

                await pesmaa.Set("p.ZbirkaPesama = \'"+ pesma.ZbirkaPesama + "\'").ExecuteWithoutResultsAsync();
            }

             if(pesma.GodinaObjavljivanja != "" && pesma.GodinaObjavljivanja != pesmaKojaSeMenja.GodinaObjavljivanja){

                await pesmaa.Set("p.GodinaObjavljivanja = \'"+ pesma.GodinaObjavljivanja + "\'").ExecuteWithoutResultsAsync();
            }
            if (pesma.GodinaObjavljivanja == null) {
                await pesmaa.Set("p.GodinaObjavljivanja = \'\'").ExecuteWithoutResultsAsync();
            }

             if(pesma.TekstPesme != "" && pesma.TekstPesme != pesmaKojaSeMenja.TekstPesme){

                await pesmaa.Set("p.TekstPesme = $tekstPesme").WithParam("tekstPesme", pesma.TekstPesme).ExecuteWithoutResultsAsync();
            }

            if (pesma.TekstPesme == null) {
                await pesmaa.Set("p.TekstPesme = \'\'").ExecuteWithoutResultsAsync();
            }

            /*if (pesma.Autor != "" && pesma.Autor != pesmaKojaSeMenja.Autor){  //treba izbrisati vezu prema prethodnom autoru i dodati vezu ka novom

                await pesmaa.Set("p.Autor = \'"+ pesma.Autor + "\'").ExecuteWithoutResultsAsync();

                await _client.Cypher.Match("(p:Pesnik {Id:\'" + Pesnik.ID(pesmaKojaSeMenja.Autor)+"\'})")
                .Match("(m:Pesma {Id:\'"+pesma.ID()+"\'})")
                .Match("(p)-[r:AUTOR]->(m)")
                .Delete("r").ExecuteWithoutResultsAsync(); 

                var pesnik = await _client.Cypher.Match("(p:Pesnik {Id:\'" + Pesnik.ID(pesma.Autor)+"\'})").Return(p=> p.As<Pesnik>()).ResultsAsync;
                if(pesnik.FirstOrDefault() != default)
                {
                    await _client.Cypher.Match("(p:Pesnik {Id:\'" + Pesnik.ID(pesma.Autor)+"\'})").Match("(m:Pesma {Id:\'"+pesma.ID()+"\'})")
                    .Create("(p)-[:AUTOR]->(m)").ExecuteWithoutResultsAsync();
                }
                else //kreiram pesnika
                {
                    
                }
            }*/

            pesmaKojaSeMenja.Epoha = (await _client.Cypher.Match("(e:Epoha)<-[:EPOHA]-(m:Pesma{Id:\'"+pesmaKojaSeMenja.ID()+"\'})").Return(e=>e.As<Epoha>()).ResultsAsync).FirstOrDefault();
            if (pesmaKojaSeMenja.Epoha == default) {
                pesmaKojaSeMenja.Epoha = null;
            }
            
            if (pesma.Epoha.NazivEpohe != null && pesma.Epoha.NazivEpohe != "") {
                 var epoha = _client.Cypher.Match("(e:Epoha {NazivEpohe:\'" + pesma.Epoha.NazivEpohe+"\'})");
                 var rezultat = await epoha.Return((e) =>e.As<Epoha>()).ResultsAsync;

                 if(rezultat.FirstOrDefault() == default){ //ako prosledjena epoha ne postoji, kreiram je
                     _client.Cypher.Create("(e:Epoha {NazivEpohe: \'" + pesma.Epoha.NazivEpohe +  "\'})").ExecuteWithoutResultsAsync().Wait();
                     await _client.Cypher.Match("(e:Epoha {NazivEpohe:\'" + pesma.Epoha.NazivEpohe+"\'})").Match("(m:Pesma {Id:\'"+pesma.ID()+"\'})")
                        .Create("(e)<-[:EPOHA]-(m)").ExecuteWithoutResultsAsync();
                 }
                 else {
                      await _client.Cypher.Match("(e:Epoha {NazivEpohe:\'" + pesma.Epoha.NazivEpohe+"\'})").Match("(m:Pesma {Id:\'"+pesma.ID()+"\'})")
                        .Merge("(e)<-[:EPOHA]-(m)").ExecuteWithoutResultsAsync();
                 }

                if (pesmaKojaSeMenja.Epoha != null) {
                    if(pesmaKojaSeMenja.Epoha.NazivEpohe != pesma.Epoha.NazivEpohe){ //treba izbrisati vezu prema prethodnoj epohi i dodati vezu ka novoj

                        await _client.Cypher.Match("(e:Epoha {NazivEpohe:\'" + pesmaKojaSeMenja.Epoha.NazivEpohe+"\'})")
                        .Match("(m:Pesma {Id:\'"+pesmaKojaSeMenja.ID()+"\'})")
                        .Match("(e)<-[r:EPOHA]-(m)")
                        .Delete("r").ExecuteWithoutResultsAsync();


                    }
                }
                 
            }
            else if (pesma.Epoha.NazivEpohe == null) {
                if (pesmaKojaSeMenja.Epoha != null) {

                        await _client.Cypher.Match("(e:Epoha {NazivEpohe:\'" + pesmaKojaSeMenja.Epoha.NazivEpohe+"\'})")
                        .Match("(m:Pesma {Id:\'"+pesmaKojaSeMenja.ID()+"\'})")
                        .Match("(e)<-[r:EPOHA]-(m)")
                        .Delete("r").ExecuteWithoutResultsAsync();

                }
            }
            return "Pesma uspesno izmenjena!";

        }


        [HttpDelete]
        [Route("ObrisiPesmu/{nazivPesme}/{Autor}/{Username}")]

        public async Task<string> ObrisiPesmu([FromRoute]string nazivPesme, [FromRoute] string Autor, [FromRoute]string Username){

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return "Los token";
            }
            await _client.ConnectAsync();

            var pesme = await _client.Cypher.Match("(p:Pesma{Id: $id})").WithParam("id",Pesma.ID(Autor, nazivPesme)).Return(p=>p.As<Pesma>()).ResultsAsync;
            if(pesme.FirstOrDefault() == default){
                return "Ne postoji";
            }

            await _client.Cypher.Match("(p:Pesma{Id: $id})").WithParam("id",Pesma.ID(Autor, nazivPesme)).DetachDelete("p").ExecuteWithoutResultsAsync();
            return "Uspesno";
        }



    }

}