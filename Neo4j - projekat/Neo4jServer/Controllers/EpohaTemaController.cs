using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Neo4jClient;
using Neo4jServer.Models;

namespace Neo4jServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EpohaTemaController : ControllerBase
    {
        private readonly IGraphClient _client;

        public EpohaTemaController(IGraphClient client)
        {
            _client = client;
        }


        private async Task<bool> Token(string Username, string authorization) {

            await _client.ConnectAsync();
            var token  = await _client.Cypher.Match("(t:Token {Username:$username})").WithParam("username",Username).Return((t) => t.As<Token>()).ResultsAsync;
            bool fleg = false;

            var dict = new Dictionary<string, Object>();
            dict.Add("username",Username);
            
            foreach(var t in token) {
                dict.Add("tokenString",t.TokenString);

                if (t.TokenString == authorization) {
                    if (new DateTime(t.VremeKreiranja).AddHours(6).Ticks < DateTime.UtcNow.Ticks) {
                        await _client.Cypher.Match("(t:Token {Username:$username,TokenString:$tokenString})").WithParams(dict).DetachDelete("t").ExecuteWithoutResultsAsync();
                    }
                    else {
                        fleg = true;
                        break;
                    }    
                }
                else {
                    if (new DateTime(t.VremeKreiranja).AddHours(6).Ticks < DateTime.UtcNow.Ticks) {
                        await _client.Cypher.Match("(t:Token {Username:$username,TokenString:$tokenString})").WithParams(dict).DetachDelete("t").ExecuteWithoutResultsAsync();
                    }
                }

                dict.Remove("tokenString");
            }
            return fleg;

        }

        [HttpPost]
        [Route("DodajNovuEpohu/{Username}")]
        public async Task<string> DodajNovuEpohu([FromBody] Epoha novaEpoha, [FromRoute] string Username) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);
            if (fleg == false) {
                return "Los token";
            }

            await _client.ConnectAsync();
            var epohe = await _client.Cypher.Match("(e:Epoha {NazivEpohe: \'" + novaEpoha.NazivEpohe + "\'})").Return(e=>e.As<Epoha>()).ResultsAsync;
            if (epohe.FirstOrDefault() != default) {
                return "Vec postoji";
            }
            _client.Cypher.Create("(e:Epoha {NazivEpohe: \'" + novaEpoha.NazivEpohe + "\', TrajanjeEpohe: \'" + 
             novaEpoha.TrajanjeEpohe + "\'})").ExecuteWithoutResultsAsync().Wait();
             return "Uspesno";

        }

        [HttpPost]
        [Route("IzmeniEpohu/{Username}")]
        public async Task<string> IzmeniEpohu([FromBody] Epoha novaEpoha, [FromRoute] string Username) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);
            if (fleg == false) {
                return "Los token";
            }

            await _client.ConnectAsync();
            var epohe = await _client.Cypher.Match("(e:Epoha {NazivEpohe: \'" + novaEpoha.NazivEpohe + "\'})").Return(e=>e.As<Epoha>()).ResultsAsync;
            if (epohe.FirstOrDefault() == default) {
                return "Ne postoji";
            }
            if (novaEpoha.TrajanjeEpohe == "") {
                novaEpoha.TrajanjeEpohe = null;
            }
            await _client.Cypher.Match("(e:Epoha {NazivEpohe: \'" + novaEpoha.NazivEpohe + "\'})").Set("e.TrajanjeEpohe=\'" + novaEpoha.TrajanjeEpohe + "\'" )
                .Return((e) => e.As<Epoha>()).ResultsAsync;
             return "Uspesno";

        }

        [HttpDelete]
        [Route("ObrisiEpohu/{nazivEpohe}/{Username}")]
        public async Task<string> ObrisiEpohu([FromRoute] string nazivEpohe, [FromRoute] string Username) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return "Los token";
            }

            await _client.ConnectAsync();
            var epohe = await _client.Cypher.Match("(e:Epoha {NazivEpohe: \'" + nazivEpohe + "\'})").Return(e=>e.As<Epoha>()).ResultsAsync;
            if (epohe.FirstOrDefault() == default) {
                return "Ne postoji";
            }

            await _client.Cypher.Match("(e:Epoha {NazivEpohe: \'" + nazivEpohe + "\'})").DetachDelete("e").ExecuteWithoutResultsAsync();

            return "Uspesno";

        }

        [HttpPost]
        [Route("DodajNovuTemu/{Username}")]
        public async Task<string> DodajNovuTemu([FromBody] Tema novaTema, [FromRoute] string Username) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return "Los token";
            }
            await _client.ConnectAsync();
            _client.Cypher.Merge("(t:Tema {NazivTeme: \'" +novaTema.NazivTeme + "\'})").ExecuteWithoutResultsAsync().Wait();

            return "Uspesno";
        }

        [HttpGet]
        [Route("VratiSveEpohe")]
        public async Task<List<Epoha>> VratiSveEpohe() {

            await _client.ConnectAsync();
            var result = await _client.Cypher.Match("(e:Epoha)").Return((e) => e.As<Epoha>()).ResultsAsync;
            var lista = new List<Epoha>();
            foreach(var epoha in result) {
                lista.Add(epoha);
            }
            return lista;

        }

        [HttpGet]
        [Route("VratiSveTeme/{Username}")]
        public async Task<List<Tema>> VratiSveTeme([FromRoute] string Username) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return null;
            }

            await _client.ConnectAsync();
            var result = await _client.Cypher.Match("(e:Tema)").Return((e) => e.As<Tema>()).ResultsAsync;
            var lista = new List<Tema>();
            foreach(var tema in result) {
                lista.Add(tema);
            }
            return lista;

        }

        [HttpGet]
        [Route("VratiPesmePoTemi/{nazivTeme}")]
        public async Task<List<Pesma>> VratiPesmePoTemi([FromRoute] string nazivTeme) {
            await _client.ConnectAsync();
            var tema = await _client.Cypher.OptionalMatch("(t:Tema{NazivTeme:\'"+nazivTeme+"\'})").Return((t) => new {tema = t.As<Tema>()}).ResultsAsync;
            if (tema.First().tema != null) {
                var pesme = await _client.Cypher.OptionalMatch("(t:Tema{NazivTeme:\'"+nazivTeme+"\'}) -[:TEMA]-> (m:Pesma)")
                .Return((t,r,m) => new {pesma = m.As<Pesma>()}).ResultsAsync;
                tema.First().tema.PesmeNaTemu = new List<Pesma>();
                foreach(var m in pesme) {
                    tema.First().tema.PesmeNaTemu.Add(m.pesma);
                }
                return tema.First().tema.PesmeNaTemu;
            }
            return new List<Pesma>();
        }

        [HttpGet]
        [Route("VratiPesmePoTemiPopularnosti/{nazivTeme}")]
        public async Task<List<Pesma>> VratiPesmePoTemiPopularnosti([FromRoute] string nazivTeme) {
            await _client.ConnectAsync();
            var tema = await _client.Cypher.OptionalMatch("(t:Tema{NazivTeme:\'"+nazivTeme+"\'})").Return((t) => new {tema = t.As<Tema>()}).ResultsAsync;
            if (tema.First().tema != null) {
                var pesme = await _client.Cypher.OptionalMatch("(t:Tema{NazivTeme:\'"+nazivTeme+"\'}) -[:TEMA]-> (m:Pesma)").Return((t,r,m) => new {pesma = m.As<Pesma>()}).OrderByDescending("m.Popularnost").Limit(10)
                .ResultsAsync;
                tema.First().tema.PesmeNaTemu = new List<Pesma>();
                foreach(var m in pesme) {
                    tema.First().tema.PesmeNaTemu.Add(m.pesma);
                }
                return tema.First().tema.PesmeNaTemu;
            }
            return new List<Pesma>();
        }


        [HttpGet]
        [Route("VratiPesnikePoEpohiPopularnosti/{nazivEpohe}")]
        public async Task<List<Pesnik>> VratiPesnikePoEpohiPopularnosti([FromRoute] string nazivEpohe) {
            await _client.ConnectAsync();
            var epoha = await _client.Cypher.OptionalMatch("(t:Epoha{NazivEpohe:\'"+nazivEpohe+"\'})").Return((t) => new {epoha = t.As<Epoha>()}).ResultsAsync;
            if (epoha.First().epoha != null) {
                var pesnik = await _client.Cypher.OptionalMatch("(t:Epoha{NazivEpohe:\'"+nazivEpohe+"\'}) <-[:EPOHA]- (m:Pesnik)").Return((t,r,m) => new {pesnik = m.As<Pesnik>()}).OrderByDescending("m.Popularnost").Limit(10)
                .ResultsAsync;
                epoha.First().epoha.PesniciEpohe = new List<Pesnik>();
                foreach(var m in pesnik) {
                    epoha.First().epoha.PesniciEpohe.Add(m.pesnik);
                }
                return epoha.First().epoha.PesniciEpohe;
            }
            return new List<Pesnik>();
        }


        
        [HttpGet]
        [Route("VratiPesmePoEpohiPopularnosti/{nazivEpohe}")]
        public async Task<List<Pesma>> VratiPesmePoEpohiPopularnosti([FromRoute] string nazivEpohe) {
            await _client.ConnectAsync();
            var epoha = await _client.Cypher.OptionalMatch("(t:Epoha{NazivEpohe:\'"+nazivEpohe+"\'})").Return((t) => new {epoha = t.As<Epoha>()}).ResultsAsync;
            if (epoha.First().epoha != null) {
                var pesme = await _client.Cypher.OptionalMatch("(t:Epoha{NazivEpohe:\'"+nazivEpohe+"\'}) <-[:EPOHA]- (m:Pesma)").Return((t,r,m) => m.As<Pesma>()).OrderByDescending("m.Popularnost").Limit(10)
                .ResultsAsync;
                epoha.First().epoha.PesmeEpohe = new List<Pesma>();
                foreach(var m in pesme) {
                    epoha.First().epoha.PesmeEpohe.Add(m);
                }
                return epoha.First().epoha.PesmeEpohe;
            }
            return new List<Pesma>();
        }

        
        [HttpGet]
        [Route("VratiPesmePoEpohi/{nazivEpohe}")]
        public async Task<List<Pesma>> VratiPesmePoEpohi([FromRoute] string nazivEpohe) {
            await _client.ConnectAsync();
            var epoha = await _client.Cypher.OptionalMatch("(t:Epoha{NazivEpohe:\'"+nazivEpohe+"\'})").Return((t) => new {epoha = t.As<Epoha>()}).ResultsAsync;
            if (epoha.First().epoha != null) {
                var pesme = await _client.Cypher.OptionalMatch("(t:Epoha{NazivEpohe:\'"+nazivEpohe+"\'}) <-[:EPOHA]- (m:Pesma)").Return((t,r,m) => new {pesma = m.As<Pesma>()}).ResultsAsync;
                epoha.First().epoha.PesmeEpohe = new List<Pesma>();
                foreach(var m in pesme) {
                    epoha.First().epoha.PesmeEpohe.Add(m.pesma);
                }
                return epoha.First().epoha.PesmeEpohe;
            }
            return new List<Pesma>();
        }

        [HttpGet]
        [Route("PretraziEpohe/{tekstPretrage}")]
        public async Task<List<Epoha>> PretraziEpohe([FromRoute] string tekstPretrage) {
            await _client.ConnectAsync();

            string[] reciPretrage = tekstPretrage.Split(' ');
            var lista = new List<Epoha>();
            foreach(var rec in reciPretrage) {
                var epoha = await _client.Cypher.Match("(e:Epoha)").Where("e.NazivEpohe =~ \'(?i).*"+rec+".*\'").Return((e) => e.As<Epoha>()).Limit(10).ResultsAsync;
                foreach(var ep in epoha) {
                lista.Add(ep);
                }
            }
            
            return lista;
        }

        [HttpGet]
        [Route("PretraziTeme/{tekstPretrage}")]
        public async Task<List<Tema>> PretraziTeme([FromRoute] string tekstPretrage) {
            await _client.ConnectAsync();

            string[] reciPretrage = tekstPretrage.Split(' ');
            var lista = new List<Tema>();
            foreach(var rec in reciPretrage) {
                var teme = await _client.Cypher.Match("(t:Tema)").Where("t.NazivTeme =~ \'(?i).*"+rec+".*\'").Return((t) => t.As<Tema>()).Limit(10).ResultsAsync;
            
                foreach(var t in teme) {
                    lista.Add(t);
                }
            }
            return lista;
        }


        [HttpGet]
        [Route("PretraziPesmePoTemi/{tekstPretrage}")]
        public async Task<List<Pesma>> PretraziPesmePoTemi([FromRoute] string tekstPretrage) {
            await _client.ConnectAsync();

            string[] reciPretrage = tekstPretrage.Split(' ');
            var lista = new List<Tema>();
            foreach(var rec in reciPretrage) {
                var teme = await _client.Cypher.Match("(t:Tema)").Where("t.NazivTeme =~ \'(?i).*"+rec+".*\'").Return((t) => t.As<Tema>()).Limit(10).ResultsAsync;
            
                foreach(var t in teme) {
                    lista.Add(t);
                }
            }
            var listaPesama = new List<Pesma>();
            if (lista.Count > 0) {
                foreach(var t in lista) {
                    var pesme = await _client.Cypher.OptionalMatch("(t:Tema{NazivTeme:\'"+t.NazivTeme+"\'}) -[:TEMA]-> (m:Pesma)")
                    .Return((t,r,m) => m.As<Pesma>()).OrderByDescending("m.Popularnost").Limit(3).ResultsAsync;
                    foreach(var m in pesme) {
                        if (m != null) {
                            m.Teme = new List<Tema>();
                            m.Teme.Add(t);
                            listaPesama.Add(m);
                        }                        
                    }
                }
            }

            return listaPesama;
        }

        [HttpGet]
        [Route("PretraziPesmePoEpohi/{tekstPretrage}")]
        public async Task<List<Pesma>> PretraziPesmePoEpohi([FromRoute] string tekstPretrage) {
            await _client.ConnectAsync();

            string[] reciPretrage = tekstPretrage.Split(' ');
            var lista = new List<Epoha>();
            foreach(var rec in reciPretrage) {
                var epohe = await _client.Cypher.Match("(e:Epoha)").Where("e.NazivEpohe =~ \'(?i).*"+rec+".*\'").Return((e) => e.As<Epoha>()).Limit(10).ResultsAsync;
            
                foreach(var e in epohe) {
                    lista.Add(e);
                }
            }
            var listaPesama = new List<Pesma>();
            if (lista.Count > 0) {
                foreach(var e in lista) {
                    var pesme = await _client.Cypher.OptionalMatch("(e:Epoha{NazivEpohe:\'"+e.NazivEpohe+"\'}) <-[:EPOHA]- (m:Pesma)")
                    .Return((t,r,m) => m.As<Pesma>()).OrderByDescending("m.Popularnost").Limit(3).ResultsAsync;
                    foreach(var m in pesme) {
                        if (m != null) {
                            m.Epoha = e;
                            listaPesama.Add(m);
                        }                        
                    }
                }
            }

            return listaPesama;
        }

        [HttpGet]
        [Route("PretraziPesnikePoEpohi/{tekstPretrage}")]
        public async Task<List<Pesnik>> PretraziPesnikePoEpohi([FromRoute] string tekstPretrage) {
            await _client.ConnectAsync();

            string[] reciPretrage = tekstPretrage.Split(' ');
            var lista = new List<Epoha>();
            foreach(var rec in reciPretrage) {
                var epohe = await _client.Cypher.Match("(e:Epoha)").Where("e.NazivEpohe =~ \'(?i).*"+rec+".*\'").Return((e) => e.As<Epoha>()).Limit(10).ResultsAsync;
            
                foreach(var e in epohe) {
                    lista.Add(e);
                }
            }
            var listaPesnika = new List<Pesnik>();
            if (lista.Count > 0) {
                foreach(var e in lista) {
                    var pesnici = await _client.Cypher.OptionalMatch("(e:Epoha{NazivEpohe:\'"+e.NazivEpohe+"\'}) <-[:EPOHA]- (m:Pesnik)")
                    .Return((t,r,m) => m.As<Pesnik>()).OrderByDescending("m.Popularnost").Limit(3).ResultsAsync;
                    foreach(var m in pesnici) {
                        if (m != null) {
                            m.Epohe = new List<Epoha>();
                            m.Epohe.Add(e);
                            listaPesnika.Add(m);
                        }                       
                    }
                }
            }
            return listaPesnika;
        }

        [HttpDelete]
        [Route("ObrisiTemu/{nazivTeme}/{Username}")]

        public async Task<string> ObrisiTemu([FromRoute]string nazivTeme, [FromRoute]string Username){

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return "Los token";
            }
            await _client.ConnectAsync();

            var teme = await _client.Cypher.Match("(t:Tema{NazivTeme: \'" + nazivTeme + "\'})").Return(t=>t.As<Tema>()).ResultsAsync;
            if(teme.FirstOrDefault() == default){
                return "Ne postoji";
            }

            await _client.Cypher.Match("(t:Tema{NazivTeme: \'" + nazivTeme + "\'})").DetachDelete("t").ExecuteWithoutResultsAsync();
            return "Uspesno";
        }

        
        [HttpPost]
        [Route("DodajTemuPesmi/{AutorPesme}/{NazivPesme}/{Username}")]

        public async Task<string> DodajTemuPesmi([FromBody] Tema tema,[FromRoute]string AutorPesme, [FromRoute]string NazivPesme, [FromRoute]string Username){
            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return "Los token";
            }
            await _client.ConnectAsync();

            var pesma = await _client.Cypher.Match("(p:Pesma{Id: \'" + Pesma.ID(AutorPesme,NazivPesme) + "\'})" ).Return(p=> p.As<Pesma>()).ResultsAsync;

            if(pesma.FirstOrDefault() == default){
                return "Pesma ne postoji!";
            }

            //ispitivanje da li tema postoji

            var t = await _client.Cypher.Match("(t:Tema{NazivTeme: \'" + tema.NazivTeme + "\'})" ).Return(t=> t.As<Tema>()).ResultsAsync;
            if(t.FirstOrDefault() == default){ //pravim tu temu i dodajem vezu izmedju teme i pesme

               await _client.Cypher.Create("(t:Tema {NazivTeme: \'" +tema.NazivTeme + "\'})").ExecuteWithoutResultsAsync();

               await _client.Cypher.Match("(p:Pesma{Id: \'" + Pesma.ID(AutorPesme,NazivPesme) + "\'})")
               .Match("(t:Tema {NazivTeme: \'" +tema.NazivTeme + "\'})")
               .Merge("(t)-[:TEMA]->(p)").ExecuteWithoutResultsAsync();
            }
            else{ //samo dodajem vezu
                await _client.Cypher.Match("(p:Pesma{Id: \'" + Pesma.ID(AutorPesme,NazivPesme) + "\'})")
               .Match("(t:Tema {NazivTeme: \'" +tema.NazivTeme + "\'})")
               .Merge("(t)-[:TEMA]->(p)").ExecuteWithoutResultsAsync();
            }

            return "Tema uspesno dodata!";
        }

        [HttpDelete]
        [Route("ObrisiTemuPesmi/{AutorPesme}/{NazivPesme}/{Username}")] //odvezuje temu od pesme
        
        public async Task<string> ObrisiTemuPesmi([FromBody]Tema tema, [FromRoute]string AutorPesme, [FromRoute]string NazivPesme, [FromRoute]string Username){

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return "Los token";
            }
            await _client.ConnectAsync();

            var pesma = await _client.Cypher.Match("(p:Pesma{Id: \'" + Pesma.ID(AutorPesme,NazivPesme) + "\'})" ).Return(p=> p.As<Pesma>()).ResultsAsync;

            if(pesma.FirstOrDefault() == default){
                return "Pesma ne postoji!";
            }

            //ispitujem da li tema postoji
            var t = await _client.Cypher.Match("(t:Tema{NazivTeme: \'" + tema.NazivTeme + "\'})" ).Return(t=> t.As<Tema>()).ResultsAsync;
            if(t.FirstOrDefault() == default){ 
                return "Tema ne postoji!";
            }

            var zahtev = _client.Cypher.Match("(p:Pesma{Id: \'" + Pesma.ID(AutorPesme,NazivPesme) + "\'})")
            .Match("(t:Tema{NazivTeme: \'" + tema.NazivTeme + "\'})" )
            .Match("(t)-[r:TEMA]->(p)");
            var veza = await zahtev.Return((t,r,p) => t.As<Tema>()).ResultsAsync;
            if (veza.FirstOrDefault() == default) {
                return "Pesma ne sadrzi temu";
            }

            await zahtev.Delete("r").ExecuteWithoutResultsAsync();

            return "Uspesno obrisana tema pesme!";
        }
    }

}