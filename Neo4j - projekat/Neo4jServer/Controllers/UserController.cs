using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Neo4jClient;
using Neo4jServer.Models;
using System.Security.Cryptography;
using System.Text;
using System.IdentityModel;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace Neo4jServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IGraphClient _client;

        public UserController(IGraphClient client)
        {
            _client = client;
        }

        private async Task<bool> Token(string Username, string authorization) {

            await _client.ConnectAsync();
            var token  = await _client.Cypher.Match("(t:Token {Username:$username})").WithParam("username",Username).Return((t) => t.As<Token>()).ResultsAsync;
            bool fleg = false;

            var dict = new Dictionary<string, Object>();
            dict.Add("username",Username);
            
            foreach(var t in token) {
                dict.Add("tokenString",t.TokenString);

                if (t.TokenString == authorization) {
                    if (new DateTime(t.VremeKreiranja).AddHours(6).Ticks < DateTime.UtcNow.Ticks) {
                        await _client.Cypher.Match("(t:Token {Username:$username,TokenString:$tokenString})").WithParams(dict).DetachDelete("t").ExecuteWithoutResultsAsync();
                    }
                    else {
                        fleg = true;
                        break;
                    }    
                }
                else {
                    if (new DateTime(t.VremeKreiranja).AddHours(6).Ticks < DateTime.UtcNow.Ticks) {
                        await _client.Cypher.Match("(t:Token {Username:$username,TokenString:$tokenString})").WithParams(dict).DetachDelete("t").ExecuteWithoutResultsAsync();
                    }
                }

                dict.Remove("tokenString");
            }
            return fleg;

        }

        // Metoda za registraciju
        [HttpPost]  
        [Route("Registracija/{Username}")]  
        public async Task<string> Registracija([FromBody] Registracija registracioniPodaci, [FromRoute] string Username)  {  

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);
            
            if (fleg == false) {
                return "Los token";
            }

            if (registracioniPodaci.KorisnickoIme == "") {
                return "Korisnicko ime prazan string";
            }

            if (registracioniPodaci.Sifra.Length < 8) {
                return "Sifra nedovoljno duga";
            }
            

            if (registracioniPodaci.KorisnickoIme.Length > 256) {
                return "Predugi username";
            }

            if (registracioniPodaci.KorisnickoIme.Contains(" ")) {
                return "Username sadrzi razmake";
            }

            await _client.ConnectAsync();
            
            var user = await _client.Cypher.Match("(u:User {Username:\'" + registracioniPodaci.KorisnickoIme + "\'})").Return(u=>u.As<User>()).ResultsAsync;
            if (user.FirstOrDefault() != default) {
                return "Username zauzet";
            }  


            if (registracioniPodaci.Sifra != registracioniPodaci.PotvrdjenaSifra) {
                return "Sifra i potvrdjena sifra se ne poklapaju";
            }

            User noviUser = new User();
            noviUser.Username = registracioniPodaci.KorisnickoIme;
            HashAlgorithm sha = SHA256.Create();
            
            byte[] result = sha.ComputeHash(Encoding.ASCII.GetBytes(registracioniPodaci.Sifra));
            noviUser.HashPassword = Encoding.ASCII.GetString(result);

             var dict = new Dictionary<string, Object>();
                    dict.Add("username",noviUser.Username);
                    dict.Add("pass",noviUser.HashPassword);

            await _client.Cypher.Create("(u:User{Username:$username, HashPassword:$pass})").WithParams(dict).ExecuteWithoutResultsAsync();
                  
            return "Uspesno";
            

        }  

        [HttpPost]  
        [Route("RegistracijaBezAutorizacije")]  
        private async Task<string> RegistracijaBezAutorizacije([FromBody] Registracija registracioniPodaci)  {  
            

            if (registracioniPodaci.KorisnickoIme.Length > 256) {
                return "Predugi username";
            }

            if (registracioniPodaci.KorisnickoIme.Contains(" ")) {
                return "Username sadrzi razmake";
            }

            await _client.ConnectAsync();
            
            var user = await _client.Cypher.Match("(u:User {Username:\'" + registracioniPodaci.KorisnickoIme + "\'})").Return(u=>u.As<User>()).ResultsAsync;
            if (user.FirstOrDefault() != default) {
                return "Username zauzet";
            }  


            if (registracioniPodaci.Sifra != registracioniPodaci.PotvrdjenaSifra) {
                return "Sifra i potvrdjena sifra se ne poklapaju";
            }

            User noviUser = new User();
            noviUser.Username = registracioniPodaci.KorisnickoIme;
            HashAlgorithm sha = SHA256.Create();
            
            byte[] result = sha.ComputeHash(Encoding.ASCII.GetBytes(registracioniPodaci.Sifra));
            noviUser.HashPassword = Encoding.ASCII.GetString(result);

            await _client.Cypher.Create("(u:User{Username:\'" + noviUser.Username + "\', HashPassword:\'" + noviUser.HashPassword + "\'})").ExecuteWithoutResultsAsync();
                  
            return "Uspesno";
            

        }  

        // Metoda za prijavljivanje
        [HttpPost]  
        [Route("Prijava")]  
        public async Task<string> Prijava([FromBody] Prijavljivanje podaciPrijavljivanja) {


            HashAlgorithm sha = SHA256.Create();
            
            byte[] result = sha.ComputeHash(Encoding.ASCII.GetBytes(podaciPrijavljivanja.Sifra));
            var hesiranaSifra = Encoding.ASCII.GetString(result);

            await _client.ConnectAsync();

            var user = await _client.Cypher.Match("(u:User {Username:\'" + podaciPrijavljivanja.KorisnickoIme + "\'})").Return(u=>u.As<User>()).ResultsAsync;
            if (user.FirstOrDefault() == default) {
                return "Ne postoji";
            }  

            if (user.First().HashPassword != hesiranaSifra) {
                return "Uneta pogresna sifra";
            }

            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("ByYM000OLlMQG6VVVp1OH7Xzyr7gHuw1qvUC5dcGt3SNM"));  
  
            var token = new JwtSecurityToken(  
 
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)

            );  
  
                
            var  tokenString = new JwtSecurityTokenHandler().WriteToken(token);

            Token noviToken = new Token();
            noviToken.TokenString = tokenString;
            noviToken.Username = podaciPrijavljivanja.KorisnickoIme;
            noviToken.VremeKreiranja = DateTime.UtcNow.Ticks;

            await _client.Cypher.Create("(t:Token{TokenString:\'" + noviToken.TokenString + "\', Username:\'"+ noviToken.Username + "\', VremeKreiranja:\'"+ noviToken.VremeKreiranja +"\'})").ExecuteWithoutResultsAsync();

            return tokenString;

        }

        [HttpPost]  
        [Route("PromenaSifre/{Username}")]  
        public async Task<string> PromenaSifre([FromBody] Registracija registracioniPodaci, [FromRoute] string Username)  {  

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);
            
            if (fleg == false) {
                return "Los token";
            }
            

            await _client.ConnectAsync();
            
            var user = await _client.Cypher.Match("(u:User {Username:\'" + Username + "\'})").Return(u=>u.As<User>()).ResultsAsync;
            if (user.FirstOrDefault() == default) {
                return "Ne postoji";
            }  


            
            if (registracioniPodaci.Sifra.Length < 8) {
                return "Sifra nedovoljno duga";
            }

            if (registracioniPodaci.Sifra != registracioniPodaci.PotvrdjenaSifra) {
                return "Sifra i potvrdjena sifra se ne poklapaju";
            }


            HashAlgorithm sha = SHA256.Create();
            
            byte[] result = sha.ComputeHash(Encoding.ASCII.GetBytes(registracioniPodaci.StaraSifra));
            var hesiranaSifra = Encoding.ASCII.GetString(result);

            if (user.First().HashPassword != hesiranaSifra) {
                return "Uneta pogresna sifra";
            }

            result = sha.ComputeHash(Encoding.ASCII.GetBytes(registracioniPodaci.Sifra));
            hesiranaSifra = Encoding.ASCII.GetString(result);

            await _client.Cypher.Match("(u:User {Username:\'" + Username + "\'})").Set("u.HashPassword = $hesiranaSifra").WithParam("hesiranaSifra", hesiranaSifra).ExecuteWithoutResultsAsync();

            var tokeni  = await _client.Cypher.Match("(t:Token {Username:\'" + Username + "\'})").Return((t) => t.As<Token>()).ResultsAsync;
            foreach(var t in tokeni) {
                await _client.Cypher.Match("(t:Token {Username:\'" + Username + "\',TokenString:\'" + t.TokenString +"\'})").DetachDelete("t").ExecuteWithoutResultsAsync();
                
            }
            
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("ByYM000OLlMQG6VVVp1OH7Xzyr7gHuw1qvUC5dcGt3SNM"));  
  
            var token = new JwtSecurityToken(  
 
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)

            );  
  
                
            var  tokenString = new JwtSecurityTokenHandler().WriteToken(token);

            Token noviToken = new Token();
            noviToken.TokenString = tokenString;
            noviToken.Username = Username;
            noviToken.VremeKreiranja = DateTime.UtcNow.Ticks;

             var tokeniPostojeci  = await _client.Cypher.Match("(t:Token {Username:\'" + Username + "\'})").Return((t) => t.As<Token>()).ResultsAsync;
             if (tokeniPostojeci.FirstOrDefault() != default) {
                 await _client.Cypher.Match("(t:Token {Username:\'" + Username + "\'})").Delete("t").ExecuteWithoutResultsAsync();
             }

            await _client.Cypher.Create("(t:Token{TokenString:\'" + noviToken.TokenString + "\', Username:\'"+ noviToken.Username + "\', VremeKreiranja:\'"+ noviToken.VremeKreiranja +"\'})").ExecuteWithoutResultsAsync();

            return tokenString;

        }

    }

}