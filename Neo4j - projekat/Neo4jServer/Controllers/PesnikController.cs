using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Neo4jClient;
using Neo4jServer.Models;

namespace Neo4jServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PesnikController : ControllerBase
    {
        private readonly IGraphClient _client;

        public PesnikController(IGraphClient client)
        {
            _client = client;
        }

        private async Task<bool> Token(string Username, string authorization) {

            await _client.ConnectAsync();
            var token  = await _client.Cypher.Match("(t:Token {Username:$username})").WithParam("username",Username).Return((t) => t.As<Token>()).ResultsAsync;
            bool fleg = false;

            var dict = new Dictionary<string, Object>();
            dict.Add("username",Username);
            
            foreach(var t in token) {
                dict.Add("tokenString",t.TokenString);

                if (t.TokenString == authorization) {
                    if (new DateTime(t.VremeKreiranja).AddHours(6).Ticks < DateTime.UtcNow.Ticks) {
                        await _client.Cypher.Match("(t:Token {Username:$username,TokenString:$tokenString})").WithParams(dict).DetachDelete("t").ExecuteWithoutResultsAsync();
                    }
                    else {
                        fleg = true;
                        break;
                    }    
                }
                else {
                    if (new DateTime(t.VremeKreiranja).AddHours(6).Ticks < DateTime.UtcNow.Ticks) {
                        await _client.Cypher.Match("(t:Token {Username:$username,TokenString:$tokenString})").WithParams(dict).DetachDelete("t").ExecuteWithoutResultsAsync();
                    }
                }

                dict.Remove("tokenString");
            }
            return fleg;

        }

        [HttpPost]
        [Route("DodajNovogPesnika/{Username}")]
        public async Task<string> DodajNovogPesnika([FromBody] Pesnik noviPesnik, [FromRoute] string Username) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return "Los token";
            }
            
            await _client.ConnectAsync();
            var pesnik = await _client.Cypher.OptionalMatch("(p:Pesnik{Id:\'"+noviPesnik.ID()+"\'})").Return((p) =>  p.As<Pesnik>()).ResultsAsync;
            if (pesnik.FirstOrDefault() != default) {
                return "Vec postoji";
            }

            

            

            var dict = new Dictionary<string,Object>();
            dict.Add("id", noviPesnik.ID());
            dict.Add("imePrezime", noviPesnik.ImePrezime);
            dict.Add("biografija",noviPesnik.Biografija);


            _client.Cypher.Create("(p:Pesnik {Id: $id, ImePrezime: $imePrezime, Biografija: $biografija, Popularnost: 0})").WithParams(dict).ExecuteWithoutResultsAsync().Wait();

             return "Uspesno";
        }

        [HttpPut]
        [Route("DodajSlikuPesniku/{ImePrezime}/{Username}")]
        public async Task<string> DodajSlikuPesniku([FromForm] IFormFile slika, [FromRoute] string ImePrezime, [FromRoute] string Username) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return "Los token";
            }
            
            await _client.ConnectAsync();
            var pesnik = await _client.Cypher.OptionalMatch("(p:Pesnik{Id:$id})").WithParam("id",Pesnik.ID(ImePrezime)).Return((p) =>  p.As<Pesnik>()).ResultsAsync;
            if (pesnik.FirstOrDefault() == default) {
                return "Ne postoji";
            }



                var slikaMem = new MemoryStream();
                slika.CopyTo(slikaMem);
                byte[] slikaUBajtovima = slikaMem.ToArray();

            await _client.Cypher.Match("(p:Pesnik{Id:$id})").WithParam("id",Pesnik.ID(ImePrezime)).Set("p.Slika=$slika").WithParam("slika",slikaUBajtovima).ExecuteWithoutResultsAsync();

            return "Uspesno";

        }

        [HttpPut]
        [Route("UkloniSlikuPesniku/{ImePrezime}/{Username}")]
        public async Task<string> UkloniSlikuPesniku([FromRoute] string ImePrezime, [FromRoute] string Username) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return "Los token";
            }
            
            await _client.ConnectAsync();
            var pesnik = await _client.Cypher.OptionalMatch("(p:Pesnik{Id:$id})").WithParam("id",Pesnik.ID(ImePrezime)).Return((p) =>  p.As<Pesnik>()).ResultsAsync;
            if (pesnik.FirstOrDefault() == default) {
                return "Ne postoji";
            }


            await _client.Cypher.Match("(p:Pesnik{Id:$id})").WithParam("id",Pesnik.ID(ImePrezime)).Set("p.Slika=null").ExecuteWithoutResultsAsync();

            return "Uspesno";

        }

        /*[HttpPut]
        [Route("IzmeniPesnika/{Username}")]
        public async Task<string> IzmeniPesnika([FromBody] Pesnik IzmenjenPesnik, [FromRoute] string Username) {

            // StringValues x;
            // HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            // string authorization = x.ToString();

            // bool fleg = await Token(Username, authorization);
            
            await _client.ConnectAsync();
            var pesnik = await _client.Cypher.OptionalMatch("(p:Pesnik{Id:\'"+Pesnik.ID(IzmenjenPesnik.ImePrezime)+"\'})").Return((p) => new {pesnik = p.As<Pesnik>()}).ResultsAsync;
            if (pesnik.First().pesnik == null) {
                //return pesnik.First().pesnik;
                return "Nije pronadjen";
            }
            // _client.Cypher.Merge("(p:Pesnik {Id: \'" +  IzmenjenPesnik.ID() + "\', ImePrezime: \'" +  IzmenjenPesnik.ImePrezime + 
            // "\', Biografija: \'" +  IzmenjenPesnik.Biografija).ExecuteWithoutResultsAsync().Wait();
            //_client.Cypher.OptionalMatch("(p:Pesnik {Id: \'" +  IzmenjenPesnik.ID()).Set("(p").ExecuteWithoutResultsAsync().Wait();
            await _client.Cypher.Match("(p:Pesnik{Id:\'"+IzmenjenPesnik.ID()+"\'})").Set("p.Biografija = \"" + IzmenjenPesnik.Biografija + "\"").Return(p => new {pesnik = p.As<Pesnik>()}).ResultsAsync;


            return "Uspesno";
        }*/

        [HttpGet]
        [Route("VratiSvePesnike/{Username}")]
        public async Task<List<Pesnik>> VratiPesnike([FromRoute] string Username) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            var lista = new List<Pesnik>();

            if (fleg == false) {
                return lista;
            }

            await _client.ConnectAsync();
            
            var result = await _client.Cypher.Match("(p:Pesnik)").Return((p) =>p.As<Pesnik>()).ResultsAsync;
            foreach(var p in result) {
                lista.Add(p);
            }

            return lista;
        }

        [HttpPost]
        [Route("DodajBiografijuPesnika/{ImePrezime}/{Username}")]
        public async Task<string> DodajBiografijuPesnika([FromRoute] string ImePrezime, [FromRoute] string Username, [FromBody]StringReq biografija) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return "Los token";
            }

            await _client.ConnectAsync();
            var pesnik = await _client.Cypher.OptionalMatch("(p:Pesnik{Id:$id})").WithParam("id",Pesnik.ID(ImePrezime)).Return((p) =>  p.As<Pesnik>()).ResultsAsync;
            if (pesnik.FirstOrDefault() == default) {
                return "Ne postoji";
            }

            if (biografija.Text == "") {
                return "Uspesno";
            }

            await _client.Cypher.Match("(p:Pesnik{Id:$id})").WithParam("id",Pesnik.ID(ImePrezime)).Set("p.Biografija = $biografija").WithParam("biografija",biografija.Text).Return(p => new {pesnik = p.As<Pesnik>()}).ResultsAsync;
            return "Uspesno";

        }

        [HttpPost]
        [Route("DodajPesnikaEpohi/{ImePrezime}/{NazivEpohe}/{Username}")]
        public async Task<string> DodajPesnikaEpohi([FromRoute] string ImePrezime, [FromRoute] string NazivEpohe, [FromRoute] string Username) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return "Los token";
            }

            await _client.ConnectAsync();

            var pesnik = await _client.Cypher.Match("(p:Pesnik{Id:\'"+Pesnik.ID(ImePrezime)+"\'})").Return(p=>p.As<Pesnik>()).ResultsAsync;

            if (pesnik.FirstOrDefault() == default) {
                return "Ne postoji";
            }

            var epoha = await _client.Cypher.Match("(e:Epoha)<-[:EPOHA]-(p:Pesnik{Id:$id})").WithParam("id",Pesnik.ID(ImePrezime)).Return(e => e.As<Epoha>()).ResultsAsync;
                /*if (epoha.FirstOrDefault() != default) {    
                    var dict = new Dictionary<string, Object>();
                    dict.Add("nazivEpohe",epoha.FirstOrDefault().NazivEpohe);
                    dict.Add("id",Pesnik.ID(ImePrezime));
                    await _client.Cypher.Match("(e:Epoha{NazivEpohe:$nazivEpohe})<-[r:EPOHA]-(p:Pesnik{Id:$id})").WithParams(dict).Delete("r").ExecuteWithoutResultsAsync();
                }*/
            if (NazivEpohe != null && NazivEpohe != "") {
                epoha = await _client.Cypher.Match("(e:Epoha {NazivEpohe:$nazivEpohe})").WithParam("nazivEpohe",NazivEpohe).Return(e=>e.As<Epoha>()).ResultsAsync;
                if (epoha.FirstOrDefault() == default) {
                    _client.Cypher.Create("(e:Epoha {NazivEpohe: $nazivEpohe, TrajanjeEpohe: \'\'})").WithParam("nazivEpohe",NazivEpohe).ExecuteWithoutResultsAsync().Wait();
                }
                     
                await _client.Cypher.Match("(m:Pesnik {Id:$id})").WithParam("id",Pesnik.ID(ImePrezime)).Match("(e:Epoha {NazivEpohe:$nazivEpohe})").WithParam("nazivEpohe",NazivEpohe).Merge("(e)<-[:EPOHA]-(m)").ExecuteWithoutResultsAsync();
            }
            
                
            return "Uspesno";
        }

        [HttpPost]
        [Route("SkloniPesnikaIzEpohe/{ImePrezime}/{NazivEpohe}/{Username}")]
        public async Task<string> SkloniPesnikaIzEpohe([FromRoute] string ImePrezime, [FromRoute] string NazivEpohe, [FromRoute] string Username) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return "Los token";
            }

            await _client.ConnectAsync();

            var pesnik = await _client.Cypher.Match("(p:Pesnik{Id:\'"+Pesnik.ID(ImePrezime)+"\'})").Return(p=>p.As<Pesnik>()).ResultsAsync;

            if (pesnik.FirstOrDefault() == default) {
                return "Ne postoji";
            }

            var dict = new Dictionary<string, Object>();
                    dict.Add("nazivEpohe",NazivEpohe);
                    dict.Add("id",Pesnik.ID(ImePrezime));

            var epoha = await _client.Cypher.Match("(e:Epoha{NazivEpohe:$nazivEpohe})<-[:EPOHA]-(p:Pesnik{Id:$id})").WithParams(dict).Return(e => e.As<Epoha>()).ResultsAsync;
                if (epoha.FirstOrDefault() != default) {    
                    
                    await _client.Cypher.Match("(e:Epoha{NazivEpohe:$nazivEpohe})<-[r:EPOHA]-(p:Pesnik{Id:$id})").WithParams(dict).Delete("r").ExecuteWithoutResultsAsync();
                }
            
            
                
            return "Uspesno";
        }


        [HttpDelete]
        [Route("ObrisiPesnika/{ImePrezime}/{Username}")]
        public async Task<string> ObrisiPesnika([FromRoute] string ImePrezime, [FromRoute] string Username) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(Username, authorization);

            if (fleg == false) {
                return "Los token";
            }

            var pesnik = await _client.Cypher.Match("(p:Pesnik{Id:\'"+Pesnik.ID(ImePrezime)+"\'})").Return(p=>p.As<Pesnik>()).ResultsAsync;

            if (pesnik.FirstOrDefault() == default) {
                return "Ne postoji";
            }


            await _client.ConnectAsync();
            await _client.Cypher.Match("(p:Pesnik{Id:\'"+Pesnik.ID(ImePrezime)+"\'})").DetachDelete("p").ExecuteWithoutResultsAsync();
            return "Uspesno";
        }


        [HttpGet]
        [Route("VratiPesnika/{ImePrezime}")]
        public async Task<Pesnik> VratiPesnika([FromRoute] string ImePrezime) {
            await _client.ConnectAsync();
            var pesnik = await _client.Cypher.OptionalMatch("(p:Pesnik{Id:\'"+Pesnik.ID(ImePrezime)+"\'})").Return((p) => new {pesnik = p.As<Pesnik>()}).ResultsAsync;
            if (pesnik.First().pesnik == null) {
                return pesnik.First().pesnik;
            }
            var pesme = await _client.Cypher.OptionalMatch("(p:Pesnik{Id:\'"+Pesnik.ID(ImePrezime)+"\'}) -[:AUTOR]-> (m:Pesma)")
                .Return((p,r,m) => new {pesma = m.As<Pesma>()}).ResultsAsync;
            pesnik.FirstOrDefault().pesnik.Pesme = new List<Pesma>();
            foreach(var m in pesme) {
                pesnik.FirstOrDefault().pesnik.Pesme.Add(m.pesma);
            }

            // Povecava se popularnost
            await _client.Cypher.Match("(p:Pesnik{Id:\'"+Pesnik.ID(ImePrezime)+"\'})").Set("p.Popularnost = p.Popularnost + 1").Return(p => new {pesnik = p.As<Pesnik>()}).ResultsAsync;
            
            return pesnik.FirstOrDefault().pesnik;
        }


        [HttpGet]
        [Route("VratiPesnikePoPopularnosti")]
        public async Task<List<Pesnik>> VratiPesnikePoPopularnosti() {
            await _client.ConnectAsync();
            var pesnik = await _client.Cypher.OptionalMatch("(p:Pesnik)").Return((p) => p.As<Pesnik>()).OrderByDescending("p.Popularnost").Limit(10).ResultsAsync;
            var lista = new List<Pesnik>();
            foreach(var p in pesnik) {
                lista.Add(p);
            }
            return lista;
        }


        [HttpGet]
        [Route("VratiPesmePesnika/{ImePrezime}")]
        public async Task<List<Pesma>> VratiPesmePesnika([FromRoute] string ImePrezime) {

            await _client.ConnectAsync();
            var pesnik = await _client.Cypher.OptionalMatch("(p:Pesnik {Id:\'" + Pesnik.ID(ImePrezime) + "\'})").Return((p) => p.As<Pesnik>()).ResultsAsync;

            var lista = new List<Pesma>();
            if (pesnik.First() != null) {

                var pesme  = await _client.Cypher.OptionalMatch("(p:Pesnik {Id:\'" + Pesnik.ID(ImePrezime) + "\'})-[:AUTOR]->(m:Pesma)").Return((p,r,m) => m.As<Pesma>()).ResultsAsync;

                foreach(var pesma in pesme) {
                    lista.Add(pesma);
                }

                return lista;
            
            }

            return lista;

        }

        [HttpGet]
        [Route("PretraziPesnike/{tekstPretrage}")]
        public async Task<List<Pesnik>> PretraziPesnike([FromRoute] string tekstPretrage) {
            await _client.ConnectAsync();

            string[] reciPretrage = tekstPretrage.Split(' ');
            var lista = new List<Pesnik>();
            foreach(var rec in reciPretrage) {
                var pesnici = await _client.Cypher.Match("(p:Pesnik)").Where("p.ImePrezime =~ \'(?i).*"+rec+".*\'").Return((p) => p.As<Pesnik>()).Limit(20).ResultsAsync;
                
                foreach(var p in pesnici) {
                    lista.Add(p);
                }
            }
            return lista;
        }

        [HttpGet]
        [Route("PretraziPesmePesnika/{tekstPretrage}")]
        public async Task<List<Pesma>> PretraziPesmePesnika([FromRoute] string tekstPretrage) {
            await _client.ConnectAsync();

            string[] reciPretrage = tekstPretrage.Split(' ');
            var lista = new List<Pesnik>();
            var listaPesama = new List<Pesma>();
            foreach(var rec in reciPretrage) {
                var pesnici = await _client.Cypher.Match("(p:Pesnik)").Where("p.ImePrezime =~ \'(?i).*"+rec+".*\'").Return((p) => p.As<Pesnik>()).Limit(20).ResultsAsync;
                
                foreach(var p in pesnici) {
                    lista.Add(p);
                }
            }
            if (lista.Count > 0) {
                foreach(var pesnik in lista) {
                    var pesme  = await _client.Cypher.OptionalMatch("(p:Pesnik {Id:\'" + pesnik.Id + "\'})-[:AUTOR]->(m:Pesma)").Return((p,r,m) => m.As<Pesma>()).OrderByDescending("m.Popularnost").Limit(3).ResultsAsync;

                    foreach(var pesma in pesme) {
                        if (pesma != null) {
                            listaPesama.Add(pesma);
                        }
                    }

                }
            }
            

            
            return listaPesama;
        }


        [HttpGet]
        [Route("VratiPesnikeIstihEpoha/{ImePrezime}")]
        public async Task<List<Pesnik>> VratiPesnikeIstihEpoha([FromRoute] string ImePrezime) {

            await _client.ConnectAsync();
            var pesnici = await _client.Cypher.Match("(p:Pesnik{Id:\'"+Pesnik.ID(ImePrezime)+"\'})-[:EPOHA]->(e:Epoha)<-[:EPOHA]-(t:Pesnik)").Return((p,r1,e,r2,t) => t.As<Pesnik>()).OrderByDescending("t.Popularnost").Limit(20).ResultsAsync;
            var lista = new List<Pesnik>();
            foreach(var p in pesnici) {
                lista.Add(p);
            }
            return lista;


        }

        [HttpGet]
        [Route("VratiPesmeIstihEpoha/{ImePrezime}")]
        public async Task<List<Pesma>> VratiPesmeIstihEpoha([FromRoute] string ImePrezime) {

            await _client.ConnectAsync();
            var pesme = await _client.Cypher.Match("(p:Pesnik{Id:\'"+Pesnik.ID(ImePrezime)+"\'})-[:EPOHA]->(e:Epoha)<-[:EPOHA]-(t:Pesma)").Return((p,r1,e,r2,t) => t.As<Pesma>()).OrderByDescending("t.Popularnost").Limit(20).ResultsAsync;
            var lista = new List<Pesma>();
            foreach(var p in pesme) {
                lista.Add(p);
            }
            return lista;


        }

    }

    

}