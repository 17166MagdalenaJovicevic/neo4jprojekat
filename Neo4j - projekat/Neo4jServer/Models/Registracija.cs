using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Neo4jServer.Models
{    public class Registracija{

  
        public string KorisnickoIme { get; set; }

        public string Sifra { get; set; }

        public string PotvrdjenaSifra { get; set; }

        public string StaraSifra { get; set; }
        
    }

}