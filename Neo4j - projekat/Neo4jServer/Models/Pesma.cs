using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Neo4jServer.Models
{
    public class Pesma
    {
        public String Id { get; set; }
        public String NazivPesme { get; set; }
        public String Autor { get; set; }

        public Pesnik AutorRef { get; set; }
        public String GodinaObjavljivanja { get; set; }
        public String TekstPesme { get; set; }
        public String ZbirkaPesama { get; set; }
        public Epoha Epoha { get; set; }

        public List<Tema> Teme { get; set; }

        public List<Pesma> PesmeIzZbirke { get; set; }

        public long Popularnost { get; set; }

        public String ID() {
            return Pesnik.ID(Autor) + this.NazivPesme.Replace(" ","").ToLower();
        }

        public static String ID(string Autor, string NazivPesme) {
            return Pesnik.ID(Autor) + NazivPesme.Replace(" ","").ToLower();
        }

        public Pesma(Pesma m) {
            this.Id = m.Id;
            this.NazivPesme = m.NazivPesme;
            this.GodinaObjavljivanja = m.GodinaObjavljivanja;
            this.ZbirkaPesama = m.ZbirkaPesama;
            this.Popularnost = m.Popularnost;
        }

        public Pesma() {
            
        }

    }

}