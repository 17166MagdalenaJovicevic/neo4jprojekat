using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace Neo4jServer.Models
{
    public class Pesnik
    {
        public String Id { get; set; }
        public String ImePrezime { get; set; }
        public String Biografija { get; set; }

        public byte[] Slika { get; set; }

        public List<Pesma> Pesme { get; set; }

        public List<Epoha> Epohe { get; set; }

        public long Popularnost { get; set; }

        public String ID() {
            return this.ImePrezime.Replace(" ","").ToLower();
        }

        public static String ID(String ImePrezime) {
            return ImePrezime.Replace(" ","").ToLower();
        }

        public Pesnik(Pesnik p) {
            this.Id = p.Id;
            this.ImePrezime = p.ImePrezime;
            this.Biografija = p.Biografija;
            this.Popularnost = p.Popularnost;
        }

        public Pesnik() {
            
        }

    }
}