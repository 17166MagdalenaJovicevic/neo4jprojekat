using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Neo4jServer.Models
{
    public class Tema{

        public String NazivTeme { get; set; }

        public List<Pesma> PesmeNaTemu { get; set; }

        
    }

}