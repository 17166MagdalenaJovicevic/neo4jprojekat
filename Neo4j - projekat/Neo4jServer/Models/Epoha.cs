using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Neo4jServer.Models
{
    public class Epoha{

        public String NazivEpohe { get; set; }
        public String TrajanjeEpohe { get; set; }

        public List<Pesma> PesmeEpohe { get; set; }
        public List<Pesnik> PesniciEpohe { get; set; }
        
    }

}