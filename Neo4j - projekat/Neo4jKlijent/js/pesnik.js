export class Pesnik {

    constructor(id, imePrezime, biografija, popularnost, slika, epohe) {

        this.id = id;
        this.imePrezime = imePrezime;
        this.biografija = biografija;
        this.popularnost = popularnost;
        this.slika = slika;
        this.pesme = [];
        this.epohe = [];
        if (epohe != null) {
            this.epohe = epohe;
        }

    }

    PrikazPesnikaSaLinkom(host) {

        let kontejner = document.createElement("div");
        kontejner.classList.add("p-2","d-flex","KontejnerPesnikaSaLinkom");

        let card = document.createElement("div");
        card.classList.add("card-body");

        kontejner.append(card);

        let cardTitle = document.createElement("h5");
        cardTitle.classList.add("card-title");
        cardTitle.innerHTML = this.imePrezime;

        card.append(cardTitle);


        let link = document.createElement("a");
        link.href = "prikazPesnika.html?imePrezime=" + this.imePrezime;
        link.target = "_blank";
        link.rel = "noopener noreferrer";
        link.innerHTML = 'Biografija i pesme pesnika ' + '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-arrow-in-up-right" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M6.364 13.5a.5.5 0 0 0 .5.5H13.5a1.5 1.5 0 0 0 1.5-1.5v-10A1.5 1.5 0 0 0 13.5 1h-10A1.5 1.5 0 0 0 2 2.5v6.636a.5.5 0 1 0 1 0V2.5a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v10a.5.5 0 0 1-.5.5H6.864a.5.5 0 0 0-.5.5z"/><path fill-rule="evenodd" d="M11 5.5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793l-8.147 8.146a.5.5 0 0 0 .708.708L10 6.707V10.5a.5.5 0 0 0 1 0v-5z"/></svg>';
        
        card.append(link);


        host.append(kontejner);

    }

    PrikazPesnikaKaoLinkSaRednimBrojem(host, br) {

        let div = document.createElement("div");

        let rbr = document.createElement("span");
        rbr.innerHTML = br + ". ";
        div.append(rbr);

        let link = document.createElement("a");
        link.href = "prikazPesnika.html?imePrezime=" + this.imePrezime;
        link.target = "_blank";
        link.rel = "noopener noreferrer";
        link.innerHTML = this.imePrezime;

        div.append(link);


        host.append(div);

    }

    PrikazListItem(host) {

        let listItem = document.createElement("li");
        listItem.classList.add("list-group-item");

        let tip = document.createElement("span");
        tip.innerHTML = "Pesnik: ";
        listItem.append(tip);

        let link = document.createElement("a");
        link.innerHTML = this.imePrezime;
        link.href = "prikazPesnika.html?imePrezime=" + this.imePrezime;
        link.target = "_blank";
        link.rel = "noopener noreferrer";
        listItem.append(link);

        host.append(listItem);

    }

    PrikazKaoLink(host) {
        let link = document.createElement("a");
        link.classList.add("text-secondary");
        link.innerHTML = this.imePrezime;
        link.href = "prikazPesnika.html?imePrezime=" + this.imePrezime;
        link.target = "_blank";
        link.rel = "noopener noreferrer";
        host.append(link);
    }

}