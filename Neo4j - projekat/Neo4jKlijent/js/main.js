import { Pesma } from "./pesma.js";
import { Pesnik } from "./pesnik.js";
import { Epoha } from "./epoha.js";
import { Tema } from "./tema.js";

sessionStorage.setItem("webapi", "https://localhost:5001/");

var webapi = sessionStorage.getItem("webapi");

// 

Inicijalizacija();


// 

async function Inicijalizacija() {

    VratiNajpopularnijePesme();
    VratiNajpopularnijePesnike();
    VratiSveEpohe();

}


var pretrazi = document.querySelector("input[name=Pretrazi]");
pretrazi.onkeypress = (ev) => {
    if (ev.keyCode === 13) {
        pretraziDugme.click();
    }
}

let pretraziDugme = document.querySelector(".DugmePretrazi");
pretraziDugme.onclick = (ev) => {
    Pretrazi();
}

let prijaviSeDugme = document.querySelector(".DugmePrijaviSe");
prijaviSeDugme.onclick = (ev) => {
    PrijaviSe();
}

    


async function VratiNajpopularnijePesme() {

    let listaNajpopularnijihPesama = await FetchVratiNajpopularnijePesme();
    if (listaNajpopularnijihPesama == null || listaNajpopularnijihPesama == false) {
        return false;
    }

    let kontejnerNajpopularnijihPesama = document.querySelector(".NajpopularnijePesme");
    kontejnerNajpopularnijihPesama.innerHTML = "";

    listaNajpopularnijihPesama.forEach(pesma => {
        pesma.PrikazPesmeSaLinkom(kontejnerNajpopularnijihPesama);
    })

}

async function FetchVratiNajpopularnijePesme() {

    let odgovor = await fetch(webapi + "Pesma/VratiPesmePoPopularnosti", {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesama  = [];

    odgovor.forEach(pesma => {
        listaPesama.push(new Pesma(pesma.id, pesma.nazivPesme, pesma.autor, pesma.godinaObjavljivanja, pesma.tekstPesme));
    })

    return listaPesama;

}

async function VratiNajpopularnijePesnike() {

    let listaNajpopularnijihPesnika = await FetchVratiNajpopularnijePesnike();
    if (listaNajpopularnijihPesnika == null || listaNajpopularnijihPesnika == false) 
    {
        return false;
    }

    let kontejnerNajpopularnijihPesnika = document.querySelector(".NajpopularnijiPesnici");
    kontejnerNajpopularnijihPesnika.innerHTML = "";

    listaNajpopularnijihPesnika.forEach(pesnik => {
        pesnik.PrikazPesnikaSaLinkom(kontejnerNajpopularnijihPesnika);
    })

}

async function FetchVratiNajpopularnijePesnike() {

    let odgovor = await fetch(webapi + "Pesnik/VratiPesnikePoPopularnosti", {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesnika  = [];

    odgovor.forEach(pesnik => {
        listaPesnika.push(new Pesnik(pesnik.id, pesnik.imePrezime, pesnik.biografija, pesnik.popularnost, pesnik.slika));
    })

    return listaPesnika;

}

async function VratiSveEpohe() {

    let listaEpoha = await FetchVratiSveEpohe();
    if (listaEpoha == null || listaEpoha == false) {
        return false;
    }

    let kontejnerEpoha = document.querySelector(".Epohe");
    kontejnerEpoha.innerHTML = "";

    listaEpoha.forEach(epoha => {
        epoha.PrikazEpohe(kontejnerEpoha);
    })

}

async function FetchVratiSveEpohe() {

    let odgovor = await fetch(webapi + "EpohaTema/VratiSveEpohe", {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaEpoha  = [];

    odgovor.forEach(epoha => {
        listaEpoha.push(new Epoha(epoha.nazivEpohe, epoha.trajanjeEpohe));
    })

    return listaEpoha;

}




async function Pretrazi() {

    let tekstPretrage = document.querySelector("input[name=Pretrazi]").value;

    let kontejner = document.querySelector(".RezultatiPretrage");
    kontejner.innerHTML = "";
    kontejner.style.display = "block";

    if (document.querySelector("input[name=cbxPesme]").checked) {
        await PretraziPesme(tekstPretrage);
    }

    
    if (document.querySelector("input[name=cbxPesnici]").checked) {
        await PretraziPesnike(tekstPretrage);
    }
    
    if (document.querySelector("input[name=cbxEpohe]").checked) {
        await PretraziEpohe(tekstPretrage);
    }
    if (document.querySelector("input[name=cbxTeme]").checked) {
        await PretraziTeme(tekstPretrage);
    }
    if (document.querySelector("input[name=cbxPesme]").checked) {
        PretraziPesmePesnika(tekstPretrage);
        PretraziPesmePoTemi(tekstPretrage);
        PretraziPesmePoEpohi(tekstPretrage);
    }
    
    
    if (document.querySelector("input[name=cbxPesnici]").checked) {
        PretraziPesnikaPoEpohi(tekstPretrage);
    }
    

}

async function PretraziPesme(tekstPretrage) {

    let listaPesama = await FetchPretraziPesme(tekstPretrage);
    if (listaPesama == null || listaPesama == false) {
        return false;
    }

    let kontejner = document.querySelector(".RezultatiPretrage");

    listaPesama.forEach(pesma => {
        pesma.PrikazListItem(kontejner);
    })

}

async function FetchPretraziPesme(tekstPretrage) {

    let odgovor = await fetch(webapi + "Pesma/PretraziPesme/" + tekstPretrage, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesama  = [];

    odgovor.forEach(pesma => {
        listaPesama.push(new Pesma(pesma.id, pesma.nazivPesme, pesma.autor, pesma.godinaObjavljivanja, pesma.tekstPesme));
    })

    return listaPesama;

}


async function PretraziPesnike(tekstPretrage) {

    let listaPesnika = await FetchPretraziPesnike(tekstPretrage);
    if (listaPesnika == null || listaPesnika == false) 
    {
        return false;
    }

    let kontejner = document.querySelector(".RezultatiPretrage");


    listaPesnika.forEach(pesnik => {
        pesnik.PrikazListItem(kontejner);
    })

}

async function FetchPretraziPesnike(tekstPretrage) {

    let odgovor = await fetch(webapi + "Pesnik/PretraziPesnike/" + tekstPretrage, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesnika  = [];

    odgovor.forEach(pesnik => {
        listaPesnika.push(new Pesnik(pesnik.id, pesnik.imePrezime, pesnik.biografija, pesnik.popularnost, pesnik.slika));
    })

    return listaPesnika;

}


async function PretraziEpohe(tekstPretrage) {

    let listaEpoha = await FetchPretraziEpohe(tekstPretrage);
    if (listaEpoha == null || listaEpoha == false) {
        return false;
    }

    let kontejner = document.querySelector(".RezultatiPretrage");


    listaEpoha.forEach(epoha => {
        epoha.PrikazListItem(kontejner);
    })

}

async function FetchPretraziEpohe(tekstPretrage) {

    let odgovor = await fetch(webapi + "EpohaTema/PretraziEpohe/" + tekstPretrage, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaEpoha  = [];

    odgovor.forEach(epoha => {
        listaEpoha.push(new Epoha(epoha.nazivEpohe, epoha.trajanjeEpohe));
    })

    return listaEpoha;

}

async function PretraziTeme(tekstPretrage) {

    let listaTema = await FetchPretraziTeme(tekstPretrage);
    if (listaTema == null || listaTema == false) {
        return false;
    }

    let kontejner = document.querySelector(".RezultatiPretrage");


    listaTema.forEach(tema => {
        tema.PrikazListItem(kontejner);
    })

}

async function FetchPretraziTeme(tekstPretrage) {

    let odgovor = await fetch(webapi + "EpohaTema/PretraziTeme/" + tekstPretrage, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaTema  = [];

    odgovor.forEach(tema => {
        listaTema.push(new Tema(tema.nazivTeme));
    })

    return listaTema;

}


async function PretraziPesmePesnika(tekstPretrage) {

    let listaPesama = await FetchPretraziPesmePesnika(tekstPretrage);
    if (listaPesama == null || listaPesama == false) {
        return false;
    }

    let kontejner = document.querySelector(".RezultatiPretrage");

    listaPesama.forEach(pesma => {
        pesma.PrikazListItem(kontejner);
    })

}

async function FetchPretraziPesmePesnika(tekstPretrage) {

    let odgovor = await fetch(webapi + "Pesnik/PretraziPesmePesnika/" + tekstPretrage, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesama  = [];

    odgovor.forEach(pesma => {
        listaPesama.push(new Pesma(pesma.id, pesma.nazivPesme, pesma.autor, pesma.godinaObjavljivanja, pesma.tekstPesme));
    })

    return listaPesama;

}


async function PretraziPesmePoTemi(tekstPretrage) {

    let listaPesama = await FetchPretraziPesmePoTemi(tekstPretrage);
    if (listaPesama == null || listaPesama == false) {
        return false;
    }

    let kontejner = document.querySelector(".RezultatiPretrage");

    listaPesama.forEach(pesma => {
        pesma.PrikazListItemTema(kontejner);
    })

}

async function FetchPretraziPesmePoTemi(tekstPretrage) {

    let odgovor = await fetch(webapi + "EpohaTema/PretraziPesmePoTemi/" + tekstPretrage, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesama  = [];

    odgovor.forEach(pesma => {
        listaPesama.push(new Pesma(pesma.id, pesma.nazivPesme, pesma.autor, pesma.godinaObjavljivanja, pesma.tekstPesme, null, null,null,pesma.teme));
    })

    return listaPesama;

}


async function PretraziPesmePoEpohi(tekstPretrage) {

    let listaPesama = await FetchPretraziPesmePoEpohi(tekstPretrage);
    if (listaPesama == null || listaPesama == false) {
        return false;
    }

    let kontejner = document.querySelector(".RezultatiPretrage");

    listaPesama.forEach(pesma => {
        pesma.PrikazListItemEpoha(kontejner);
    })

}

async function FetchPretraziPesmePoEpohi(tekstPretrage) {

    let odgovor = await fetch(webapi + "EpohaTema/PretraziPesmePoEpohi/" + tekstPretrage, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesama  = [];

    odgovor.forEach(pesma => {
        listaPesama.push(new Pesma(pesma.id, pesma.nazivPesme, pesma.autor, pesma.godinaObjavljivanja, pesma.tekstPesme, null, pesma.epoha));
    })

    return listaPesama;

}


async function PretraziPesnikaPoEpohi(tekstPretrage) {

    let listaPesnika = await FetchPretraziPesnikaPoEpohi(tekstPretrage);
    if (listaPesnika == null || listaPesnika == false) {
        return false;
    }

    let kontejner = document.querySelector(".RezultatiPretrage");

    let naslov = document.createElement("h6");
    naslov.innerHTML = "Pesnici epohe: " + listaPesnika[0].epohe[0].nazivEpohe;
    kontejner.append(naslov);

    listaPesnika.forEach(pesnik => {
        pesnik.PrikazListItem(kontejner);
    })

    naslov = document.createElement("h6");
    naslov.classList.add("p-1");
    kontejner.append(naslov);

}

async function FetchPretraziPesnikaPoEpohi(tekstPretrage) {

    let odgovor = await fetch(webapi + "EpohaTema/PretraziPesnikePoEpohi/" + tekstPretrage, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesnika  = [];

    odgovor.forEach(pesnik => {
        listaPesnika.push(new Pesnik(pesnik.id, pesnik.imePrezime, pesnik.biografija, pesnik.popularnost, pesnik.slika, pesnik.epohe));
    })

    return listaPesnika;

}


async function PrijaviSe() {

    let fp = document.querySelector(".FormaZaPrijavljivanje");
        fp.querySelectorAll(".invalid-feedback").forEach(p => {
            p.style.display = "none";
        })

    let korisnickoIme = fp.querySelector("input[name=KorisnickoIme]").value;
    let sifra = fp.querySelector("input[name=Sifra]").value;

    if(korisnickoIme == "" || sifra == ""){

        document.querySelector(".PrijavljivanjePopuniteSvaPolja").style.display = "block";
        return;

    }


    PrijavaFetch(korisnickoIme, sifra, fp);    

}

async function PrijavaFetch(korisnickoIme, sifra, fp) {

    let odgovor = await fetch(webapi + "User/Prijava", {
        method: "POST",
        headers:{
            "Content-Type" : "application/json"
        },
        body: JSON.stringify({
            korisnickoIme : korisnickoIme,
            sifra : sifra
        })
    }).catch(reason => {

        
        return false;

    })

    if (odgovor == false){
        fp.querySelector(".PrijavljivanjeDosloJeDoGreske").style.display = "block";
        return false;
    }
        
    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        fp.querySelector(".PrijavljivanjeDosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Ne postoji") {
        document.querySelector(".PrijavljivanjeKorisnickoImeFeedback").style.display = "block";
    }
    else {
        if (odgovor == "Uneta pogresna sifra") {
            document.querySelector(".PrijavljivanjeSifraFeedback").style.display = "block";
        }
        else {

            localStorage.setItem("token",odgovor);
            localStorage.setItem("username",korisnickoIme);
            window.open("administrator.html", '_blank','noopener noreferrer');

        }
    }

}