import { Pesma } from "./pesma.js";
import { Pesnik } from "./pesnik.js";
import { Epoha } from "./epoha.js";
import { Tema } from "./tema.js";

sessionStorage.setItem("webapi", "https://localhost:5001/");

var webapi = sessionStorage.getItem("webapi");
var brEpoha = 0;
var brPesama = 0;
var brPesnika = 0;
var brTema = 0;

// 

Inicijalizacija();


// 

async function Inicijalizacija() {

   VratiSvePesme();
   VratiSveEpohe();
   VratiSvePesnike();
   VratiSveTeme();

}

document.querySelector(".DugmeOdjaviSe").onclick = (ev) => {
    localStorage.removeItem("username");
    localStorage.removeItem("token");
    window.location.href = "index.html";
}

function LosToken() {

    let pozadina = document.createElement("div");
        pozadina.classList.add("PozadinaZaGreskuPrijavljivanja", "text-light");
        pozadina.innerHTML = "Potrebno je ponovno prijavljivanje!";
        document.body.appendChild(pozadina);

        let div = document.createElement("div");
        pozadina.appendChild(div);

        let dugmePrijaviSeOpet = document.createElement("button");
        dugmePrijaviSeOpet.classList.add("btn", "text-light", "m-5");
        dugmePrijaviSeOpet.innerHTML = 'Prijavljivanje';
        div.appendChild(dugmePrijaviSeOpet);

        dugmePrijaviSeOpet.onclick = (ev) => {

            localStorage.removeItem("token");
            localStorage.removeItem("username");
            
            window.location.href = "index.html";

        }
}

async function VratiSvePesme() {

    let listaPesama = await FetchVratiSvePesme();
    if (listaPesama == null || listaPesama == false) {
        return false;
    }

    let kontejner = document.querySelector(".SvePesme");
    kontejner.innerHTML = "";

    listaPesama.forEach((pesma, br) => {
        pesma.PesmaKaoLinkSaRednimBrojem(kontejner, br + 1);
    })

    brPesama = listaPesama.length;

}

async function FetchVratiSvePesme() {

    let odgovor = await fetch(webapi + "Pesma/VratiSvePesme/" + localStorage.getItem("username"), {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json",
            "Authorization" : localStorage.getItem("token")
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesama  = [];

    odgovor.forEach(pesma => {
        listaPesama.push(new Pesma(pesma.id, pesma.nazivPesme, pesma.autor, pesma.godinaObjavljivanja, pesma.tekstPesme));
    })

    return listaPesama;

}

async function VratiSveEpohe() {

    let listaEpoha = await FetchVratiSveEpohe();
    if (listaEpoha == null || listaEpoha == false) {
        return false;
    }

    let kontejner = document.querySelector(".SveEpohe");


    listaEpoha.forEach((epoha, br) => {
        epoha.PrikazEpoheKaoLinkSaRednimBrojem(kontejner, br + 1);
    })

    brEpoha = listaEpoha.length;

}

async function FetchVratiSveEpohe() {

    let odgovor = await fetch(webapi + "EpohaTema/VratiSveEpohe", {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaEpoha  = [];

    odgovor.forEach(epoha => {
        listaEpoha.push(new Epoha(epoha.nazivEpohe, epoha.trajanjeEpohe));
    })

    return listaEpoha;

}

async function VratiSvePesnike() {

    let listaPesnika = await FetchVratiSvePesnike();
    if (listaPesnika == null || listaPesnika == false) 
    {
        return false;
    }

    let kontejner = document.querySelector(".SviPesnici");


    listaPesnika.forEach((pesnik, br) => {
        pesnik.PrikazPesnikaKaoLinkSaRednimBrojem(kontejner, br + 1);
    })

    brPesnika = listaPesnika.length;

}

async function FetchVratiSvePesnike() {

    let odgovor = await fetch(webapi + "Pesnik/VratiSvePesnike/" + localStorage.getItem("username"), {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json",
            "Authorization" : localStorage.getItem("token")
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesnika  = [];

    odgovor.forEach(pesnik => {
        listaPesnika.push(new Pesnik(pesnik.id, pesnik.imePrezime, pesnik.biografija, pesnik.popularnost, pesnik.slika));
    })

    return listaPesnika;

}

async function VratiSveTeme() {

    let listaTema = await FetchVratiSveTeme();
    if (listaTema == null || listaTema == false) {
        return false;
    }

    let kontejner = document.querySelector(".SveTeme");


    listaTema.forEach((tema, br) => {
        tema.PrikazKaoLinkSaRednimBrojem(kontejner, br + 1);
    })

    brTema = listaTema.length;

}

async function FetchVratiSveTeme() {

    let odgovor = await fetch(webapi + "EpohaTema/VratiSveTeme/" + localStorage.getItem("username"), {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json",
            "Authorization" : localStorage.getItem("token")
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaTema  = [];

    odgovor.forEach(tema => {
        listaTema.push(new Tema(tema.nazivTeme));
    })

    return listaTema;

}

// Dodaj novu epohu
let dugmeDodajNovuEpohu = document.querySelector(".DugmeDodajNovuEpohu");
dugmeDodajNovuEpohu.onclick = (ev) => {
    DodajNovuEpohu();
}

async function DodajNovuEpohu() {

    let formaDodajNovuEpohu = document.querySelector(".FormaDodajNovuEpohu");
    let nazivEpohe = formaDodajNovuEpohu.querySelector("input[name=NazivEpohe]").value;
    let trajanjeEpohe = formaDodajNovuEpohu.querySelector("input[name=TrajanjeEpohe").value;

    formaDodajNovuEpohu.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaDodajNovuEpohu.querySelector(".Uspesno").style.display = "none";

    if (nazivEpohe == "") {
        formaDodajNovuEpohu.querySelector(".UnesiteNazivEpohe").style.display = "block";
        return false;
    }

    await FetchDodajNovuEpohu(nazivEpohe, trajanjeEpohe);

}

async function FetchDodajNovuEpohu(nazivEpohe, trajanjeEpohe) {

    let formaDodajNovuEpohu = document.querySelector(".FormaDodajNovuEpohu");

    let odgovor = await fetch(webapi+"EpohaTema/DodajNovuEpohu/"+localStorage.getItem("username"),{
        method:"POST",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        },
        body: JSON.stringify({
            nazivEpohe: nazivEpohe,
            trajanjeEpohe : trajanjeEpohe
        })
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        formaDodajNovuEpohu.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }
        
    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        formaDodajNovuEpohu.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        return LosToken();
    }

    if (odgovor == "Vec postoji") {
        formaDodajNovuEpohu.querySelector(".PostojiVecEpoha").style.display = "block";
    }
    else if (odgovor == "Uspesno") {
        formaDodajNovuEpohu.querySelector(".Uspesno").style.display = "block";
        new Epoha(nazivEpohe, trajanjeEpohe).PrikazEpoheKaoLinkSaRednimBrojem(document.querySelector(".SveEpohe"), brEpoha + 1);
        brEpoha++;
    }

}

// Izmeni epohu
let dugmeIzmeniEpohu = document.querySelector(".DugmeIzmeniEpohu");
dugmeIzmeniEpohu.onclick = (ev) => {
    IzmeniEpohu();
}

async function IzmeniEpohu() {

    let formaDodajNovuEpohu = document.querySelector(".FormaIzmeniEpohu");
    let nazivEpohe = formaDodajNovuEpohu.querySelector("input[name=NazivEpohe]").value;
    let trajanjeEpohe = formaDodajNovuEpohu.querySelector("input[name=TrajanjeEpohe").value;

    formaDodajNovuEpohu.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaDodajNovuEpohu.querySelector(".Uspesno").style.display = "none";

    if (nazivEpohe == "") {
        formaDodajNovuEpohu.querySelector(".UnesiteNazivEpohe").style.display = "block";
        return false;
    }

    await FetchIzmeniEpohu(nazivEpohe, trajanjeEpohe);

}

async function FetchIzmeniEpohu(nazivEpohe, trajanjeEpohe) {

    let formaDodajNovuEpohu = document.querySelector(".FormaIzmeniEpohu");

    let odgovor = await fetch(webapi+"EpohaTema/IzmeniEpohu/"+localStorage.getItem("username"),{
        method:"POST",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        },
        body: JSON.stringify({
            nazivEpohe: nazivEpohe,
            trajanjeEpohe : trajanjeEpohe
        })
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        formaDodajNovuEpohu.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }
        
    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        formaDodajNovuEpohu.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
    }

    if (odgovor == "Ne postoji") {
        formaDodajNovuEpohu.querySelector(".NePostojiEpoha").style.display = "block";
    }
    else if (odgovor == "Uspesno") {
        formaDodajNovuEpohu.querySelector(".Uspesno").style.display = "block";

        document.querySelector(".SveEpohe").childNodes.forEach((e,i) => {
            if (i != 0 && e.querySelector("a").innerHTML.split(" ")[0] == nazivEpohe) {
                e.querySelector("a").innerHTML = nazivEpohe;
                if (trajanjeEpohe != "") {
                    e.querySelector("a").innerHTML += " " + trajanjeEpohe;
                }
            }
        })
    }

}

// Obrisi epohu
let dugmeObrisiEpohu = document.querySelector(".DugmeObrisiEpohu");
dugmeObrisiEpohu.onclick = (ev) => {
    ObrisiEpohu();
}

async function ObrisiEpohu() {

    let formaObrisiEpohu = document.querySelector(".FormaObrisiEpohu");
    let nazivEpohe = formaObrisiEpohu.querySelector("input[name=NazivEpohe]").value;

    formaObrisiEpohu.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaObrisiEpohu.querySelector(".Uspesno").style.display = "none";

    if (nazivEpohe == "") {
        formaObrisiEpohu.querySelector(".UnesiteNazivEpohe").style.display = "block";
        return false;
    }

    await FetchObrisiEpohu(nazivEpohe);

}

async function FetchObrisiEpohu(nazivEpohe) {

    let formaObrisiEpohu = document.querySelector(".FormaObrisiEpohu");

    let odgovor = await fetch(webapi+"EpohaTema/ObrisiEpohu/"+nazivEpohe+"/"+localStorage.getItem("username"),{
        method:"DELETE",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        }
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        formaObrisiEpohu.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }
        
    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        formaObrisiEpohu.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
    }

    if (odgovor == "Ne postoji") {
        formaObrisiEpohu.querySelector(".NePostojiEpoha").style.display = "block";
    }
    else if (odgovor == "Uspesno") {
        formaObrisiEpohu.querySelector(".Uspesno").style.display = "block";
        
        document.querySelector(".SveEpohe").childNodes.forEach((e,i) => {
            if (i != 0 && e.querySelector("a").innerHTML.split(" ")[0] == nazivEpohe) {
                document.querySelector(".SveEpohe").removeChild(e);
                brEpoha--;
            }
        })
    }

}

// Dodaj pesnika
let formaDodajPesnika = document.querySelector(".FormaDodajPesnika");

let dugmeIzaberiSlikuDodajPesnika = formaDodajPesnika.querySelector(".IzaberiSliku");
dugmeIzaberiSlikuDodajPesnika.onclick = (ev) => {
    formaDodajPesnika.querySelector("input[name=Slika]").click();
}

formaDodajPesnika.querySelector("input[name=Slika]").onchange = (ev) => {
    formaDodajPesnika.querySelector(".OdabranaSlika").innerHTML = formaDodajPesnika.querySelector("input[name=Slika]").value;
}

let dugmeSkloniSlikuDodajPesnika = formaDodajPesnika.querySelector(".UkloniSliku");
dugmeSkloniSlikuDodajPesnika.onclick = (ev) => {
    formaDodajPesnika.querySelector(".fileInputSlika").removeChild(formaDodajPesnika.querySelector(".fileInputSlika").querySelector("input"));
    formaDodajPesnika.querySelector(".fileInputSlika").innerHTML = '<input type="file" accept="image/*" class="form-control" name="Slika" style="display: none;">';
    formaDodajPesnika.querySelector("input[name=Slika]").onchange = (ev) => {
        formaDodajPesnika.querySelector(".OdabranaSlika").innerHTML = formaDodajPesnika.querySelector("input[name=Slika]").value;
    }
    formaDodajPesnika.querySelector(".OdabranaSlika").innerHTML = "";
}

let dugmeDodajPesnika = document.querySelector(".DugmeDodajNovogPesnika");
dugmeDodajPesnika.onclick = (ev) => {
    DodajNovogPesnika();
}

async function DodajNovogPesnika() {

    let formaDodajPesnika = document.querySelector(".FormaDodajPesnika");
    let imePrezimePesnika = formaDodajPesnika.querySelector("input[name=ImePrezimePesnika]").value;
    let biografijaPesnika = formaDodajPesnika.querySelector("textarea[name=BiografijaPesnika]").value;
    let Slika = null;

    if (formaDodajPesnika.querySelector(".OdabranaSlika").innerHTML != "") {
        Slika = new FormData();
        Slika.append("slika", formaDodajPesnika.querySelector("input[name=Slika]").files[0]);
    }
    

    formaDodajPesnika.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaDodajPesnika.querySelector(".Uspesno").style.display = "none";

    if (imePrezimePesnika == "") {
        formaDodajPesnika.querySelector(".UnesiteImePrezimePesnika").style.display = "block";
        return false;
    }
    if (biografijaPesnika == "") {
        formaDodajPesnika.querySelector(".UnesiteBiografijuPesnika").style.display = "block";
        return false;
    }
    

    await FetchDodajNovogPesnika(imePrezimePesnika, biografijaPesnika, Slika);

}

async function FetchDodajNovogPesnika(imePrezimePesnika, biografijaPesnika, slika) {

    let formaDodajPesnika = document.querySelector(".FormaDodajPesnika");

    let odgovor = await fetch(webapi+"Pesnik/DodajNovogPesnika/"+localStorage.getItem("username"),{
        method:"POST",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token"),
        },
        body: JSON.stringify({
            imePrezime : imePrezimePesnika,
            biografija : biografijaPesnika,
        })
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        formaDodajPesnika.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }
        
    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        formaDodajPesnika.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
    }

    if (odgovor == "Vec postoji") {
        formaDodajPesnika.querySelector(".PostojiVecPesnik").style.display = "block";
    }
    else if (odgovor == "Uspesno") {
        
        new Pesnik(null, imePrezimePesnika).PrikazPesnikaKaoLinkSaRednimBrojem(document.querySelector(".SviPesnici"),brPesnika + 1);
        brPesnika++;
        if (slika != null) {
            await FetchDodajSlikuPesniku(slika, imePrezimePesnika);
        }    
        formaDodajPesnika.querySelector(".Uspesno").style.display = "block";
    }
}

async function FetchDodajSlikuPesniku(slika, imePrezimePesnika) {

    let odgovor = await fetch(webapi+"Pesnik/DodajSlikuPesniku/"+imePrezimePesnika+"/"+localStorage.getItem("username"),{
        method:"PUT",
        headers:{
            "Authorization": localStorage.getItem("token"),
            "Accept" : "image/*"
        },
        body: slika
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        return false;
    }

}

async function FetchUkloniSlikuPesniku(imePrezimePesnika) {

    let odgovor = await fetch(webapi+"Pesnik/UkloniSlikuPesniku/"+imePrezimePesnika+"/"+localStorage.getItem("username"),{
        method:"PUT",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        }
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        return false;
    }

}

// Izmeni pesnika

let formaIzmeniPesnika = document.querySelector(".FormaIzmeniPesnika");

let dugmeIzaberiSlikuIzmeniPesnika = formaIzmeniPesnika.querySelector(".IzaberiSliku");
dugmeIzaberiSlikuIzmeniPesnika.onclick = (ev) => {
    formaIzmeniPesnika.querySelector("input[name=Slika]").click();
}

formaIzmeniPesnika.querySelector("input[name=Slika]").onchange = (ev) => {
    formaIzmeniPesnika.querySelector(".OdabranaSlika").innerHTML = formaIzmeniPesnika.querySelector("input[name=Slika]").value;
}

let dugmeSkloniSlikuIzmeniPesnika = formaIzmeniPesnika.querySelector(".UkloniSliku");
dugmeSkloniSlikuIzmeniPesnika.onclick = (ev) => {
    formaIzmeniPesnika.querySelector(".fileInputSlika").removeChild(formaIzmeniPesnika.querySelector(".fileInputSlika").querySelector("input"));
    formaIzmeniPesnika.querySelector(".fileInputSlika").innerHTML = '<input type="file" accept="image/*" class="form-control" name="Slika" style="display: none;">';
    formaIzmeniPesnika.querySelector("input[name=Slika]").onchange = (ev) => {
        formaIzmeniPesnika.querySelector(".OdabranaSlika").innerHTML = formaIzmeniPesnika.querySelector("input[name=Slika]").value;
    }
    formaIzmeniPesnika.querySelector(".OdabranaSlika").innerHTML = "";
}


let dugmeIzmeniPesnika = document.querySelector(".DugmeIzmeniPesnika");
dugmeIzmeniPesnika.onclick = (ev) => {
    IzmeniPesnika();
}

async function IzmeniPesnika() {

    let formaIzmeniPesnika = document.querySelector(".FormaIzmeniPesnika");
    let imePrezimePesnika = formaIzmeniPesnika.querySelector("input[name=ImePrezimePesnika]").value;
    let biografijaPesnika = formaIzmeniPesnika.querySelector("textarea[name=BiografijaPesnika]").value;
    let obrisatiSliku = formaIzmeniPesnika.querySelector("input[name=cbxSlika]").checked;
    let Slika = null;

    if (formaIzmeniPesnika.querySelector(".OdabranaSlika").innerHTML != "") {
        Slika = new FormData();
        Slika.append("slika", formaIzmeniPesnika.querySelector("input[name=Slika]").files[0]);
    }

    formaIzmeniPesnika.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaIzmeniPesnika.querySelector(".Uspesno").style.display = "none";

    if (imePrezimePesnika == "") {
        formaIzmeniPesnika.querySelector(".UnesiteImePrezimePesnika").style.display = "block";
        return false;
    }
    

    await FetchIzmeniPesnika(imePrezimePesnika, biografijaPesnika, Slika, obrisatiSliku);

}

async function FetchIzmeniPesnika(imePrezimePesnika, biografijaPesnika, slika, obrisatiSliku) {

    let formaDodajPesnika = document.querySelector(".FormaIzmeniPesnika");

    let odgovor = await fetch(webapi+"Pesnik/DodajBiografijuPesnika/"+imePrezimePesnika+"/"+localStorage.getItem("username"),{
        method:"POST",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        },
        body: JSON.stringify({
            text : biografijaPesnika
        })
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        formaDodajPesnika.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }
        
    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        formaDodajPesnika.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
    }

    if (odgovor == "Ne postoji") {
        formaDodajPesnika.querySelector(".PesnikNijePronadjen").style.display = "block";
        return;
    }
    else if (odgovor == "Uspesno") {
        
        if (obrisatiSliku == true) {
            await FetchUkloniSlikuPesniku(imePrezimePesnika);
        }
        else {
            if (slika != null) {
                await FetchDodajSlikuPesniku(slika, imePrezimePesnika);
            }
        }

        formaDodajPesnika.querySelector(".Uspesno").style.display = "block";
    }
}

// Obrisi pesnika
let dugmeObrisiPesnika = document.querySelector(".DugmeObrisiPesnika");
dugmeObrisiPesnika.onclick = (ev) => {
    ObrisiPesnika();
}

async function ObrisiPesnika() {

    let formaDodajPesnika = document.querySelector(".FormaObrisiPesnika");
    let imePrezimePesnika = formaDodajPesnika.querySelector("input[name=ImePrezimePesnika]").value;

    formaDodajPesnika.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaDodajPesnika.querySelector(".Uspesno").style.display = "none";

    if (imePrezimePesnika == "") {
        formaDodajPesnika.querySelector(".UnesiteNazivPesnika").style.display = "block";
        return false;
    }
    
    await FetchObrisiPesnika(imePrezimePesnika);

}

async function FetchObrisiPesnika(imePrezimePesnika) {

    let formaObrisiPesnika = document.querySelector(".FormaObrisiPesnika");

    let odgovor = await fetch(webapi+"Pesnik/ObrisiPesnika/"+imePrezimePesnika + "/" + localStorage.getItem("username"),{
        method:"DELETE",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        }
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        formaObrisiPesnika.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }
        
    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        formaObrisiPesnika.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
    }

    if (odgovor == "Ne postoji") {
        formaObrisiPesnika.querySelector(".NePostojiPesnik").style.display = "block";
    }

    else if (odgovor == "Uspesno") {
        formaObrisiPesnika.querySelector(".Uspesno").style.display = "block";
        document.querySelector(".SviPesnici").childNodes.forEach((e,i) => {
            if (i != 0 && e.querySelector("a").innerHTML == imePrezimePesnika) {
                document.querySelector(".SviPesnici").removeChild(e);
                brPesnika--;
            }
        })
    }
}

// Dodavanje pesnika epohi
let dugmeDodajPesnikaEpohi = document.querySelector(".DugmeDodajPesnikaEpohi");
dugmeDodajPesnikaEpohi.onclick = (ev) => {
    DodajPesnikaEpohi();
}

async function DodajPesnikaEpohi() {

    let formaDodajPesnika = document.querySelector(".FormaDodajPesnikaEpohi");
    let imePrezimePesnika = formaDodajPesnika.querySelector("input[name=ImePrezimePesnika]").value;
    let epoha = formaDodajPesnika.querySelector("input[name=NazivEpohe]").value;

    formaDodajPesnika.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaDodajPesnika.querySelector(".Uspesno").style.display = "none";

    if (imePrezimePesnika == "") {
        formaDodajPesnika.querySelector(".UnesiteImePrezimePesnika").style.display = "block";
        return false;
    }
    if (epoha == "") {
        formaDodajPesnika.querySelector(".UnesiteNazivEpohe").style.display = "block";
        return false;
    }
    

    await FetchDodajPesnikaEpohi(imePrezimePesnika, epoha);

}

async function FetchDodajPesnikaEpohi(imePrezimePesnika, epoha) {

    let formaDodajPesnikaEpohi = document.querySelector(".FormaDodajPesnikaEpohi");

    let odgovor = await fetch(webapi+"Pesnik/DodajPesnikaEpohi/"+ imePrezimePesnika + "/" + epoha + "/" +localStorage.getItem("username"),{
        method:"POST",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        }
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        formaDodajPesnikaEpohi.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }
        
    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        formaDodajPesnikaEpohi.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
    }

    if (odgovor == "Ne postoji") {
        formaDodajPesnikaEpohi.querySelector(".NePostojiPesnik").style.display = "block";
    }

    else if (odgovor == "Uspesno") {
        formaDodajPesnikaEpohi.querySelector(".Uspesno").style.display = "block";
        
    }
}

// Sklanjanje pesnika iz epohe

let dugmeSkloniPesnikaIzEpohe = document.querySelector(".DugmeSkloniPesnikaIzEpohe");
dugmeSkloniPesnikaIzEpohe.onclick = (ev) => {
    SkloniPesnikaIzEpohe();
}

async function SkloniPesnikaIzEpohe() {
    let formaDodajPesnika = document.querySelector(".FormaSkloniPesnikaIzEpohe");
    let imePrezimePesnika = formaDodajPesnika.querySelector("input[name=ImePrezimePesnika]").value;
    let epoha = formaDodajPesnika.querySelector("input[name=NazivEpohe]").value;

    formaDodajPesnika.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaDodajPesnika.querySelector(".Uspesno").style.display = "none";

    if (imePrezimePesnika == "") {
        formaDodajPesnika.querySelector(".UnesiteImePrezimePesnika").style.display = "block";
        return false;
    }
    if (epoha == "") {
        formaDodajPesnika.querySelector(".UnesiteNazivEpohe").style.display = "block";
        return false;
    }
    

    await FetchSkloniPesnikaIzEpohe(imePrezimePesnika, epoha);
}

async function FetchSkloniPesnikaIzEpohe(imePrezimePesnika, epoha) {
    let formaDodajPesnikaEpohi = document.querySelector(".FormaSkloniPesnikaIzEpohe");

    let odgovor = await fetch(webapi+"Pesnik/SkloniPesnikaIzEpohe/"+ imePrezimePesnika + "/" + epoha + "/" +localStorage.getItem("username"),{
        method:"POST",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        }
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        formaDodajPesnikaEpohi.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }
        
    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        formaDodajPesnikaEpohi.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
    }



    else if (odgovor == "Uspesno") {
        formaDodajPesnikaEpohi.querySelector(".Uspesno").style.display = "block";
        
    }
}

// Dodavanje nove teme
let dugmeDodajNovuTemu = document.querySelector(".DugmeDodajNovuTemu");
dugmeDodajNovuTemu.onclick = (ev) => {
    DodajNovuTemu();
}

async function DodajNovuTemu() {

    let formaDodajNovuTemu = document.querySelector(".FormaDodajNovuTemu");
    let nazivTeme = formaDodajNovuTemu.querySelector("input[name=NazivTeme]").value;
    

    formaDodajNovuTemu.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaDodajNovuTemu.querySelector(".Uspesno").style.display = "none";

    if (nazivTeme == "") {
        formaDodajNovuTemu.querySelector(".UnesiteNazivEpohe").style.display = "block";
        return false;
    }
    

    await FetchDodajNovuTemu(nazivTeme);

}

async function FetchDodajNovuTemu(tema) {

    let formaDodajNovuTemu = document.querySelector(".FormaDodajNovuTemu");

    let odgovor = await fetch(webapi+"EpohaTema/DodajNovuTemu/" + localStorage.getItem("username"),{
        method:"POST",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        },
        body: JSON.stringify({
            nazivTeme : tema
        })
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        formaDodajNovuTemu.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == "Los token") {
        LosToken();
    }

    if (odgovor == "Uspesno") {
        formaDodajNovuTemu.querySelector(".Uspesno").style.display = "block";
        new Tema(tema).PrikazKaoLinkSaRednimBrojem(document.querySelector(".SveTeme"), brTema + 1)
        brTema++;
    }
}

// Obrisi temu
let dugmeObrisiTemu = document.querySelector(".DugmeObrisiTemu");
dugmeObrisiTemu.onclick = (ev) => {
    ObrisiTemu();
}

async function ObrisiTemu() {

    let formaObrisiTemu = document.querySelector(".FormaObrisiTemu");
    let nazivTeme = formaObrisiTemu.querySelector("input[name=NazivTeme]").value;

    formaObrisiTemu.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaObrisiTemu.querySelector(".Uspesno").style.display = "none";

    if (nazivTeme == "") {
        formaObrisiTemu.querySelector(".UnesiteNazivTeme").style.display = "block";
        return false;
    }

    await FetchObrisiTemu(nazivTeme);

}

async function FetchObrisiTemu(nazivTeme) {

    let formaObrisiTemu = document.querySelector(".FormaObrisiTemu");

    let odgovor = await fetch(webapi+"EpohaTema/ObrisiTemu/"+nazivTeme+"/"+localStorage.getItem("username"),{
        method:"DELETE",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        }
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        formaObrisiTemu.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }
        
    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        formaObrisiTemu.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
    }

    if (odgovor == "Ne postoji") {
        formaObrisiTemu.querySelector(".NePostojiTema").style.display = "block";
    }
    else if (odgovor == "Uspesno") {
        formaObrisiTemu.querySelector(".Uspesno").style.display = "block";
        
        document.querySelector(".SveTeme").childNodes.forEach((e,i) => {        
            if (i != 0 && (e.childNodes[1].innerHTML.split("#")[1] == nazivTeme)) {
                document.querySelector(".SveTeme").removeChild(e);
                brTema--;
            }
        })
    }

}

let dugmeDodajNovuPesmu = document.querySelector(".DugmeDodajNovuPesmu");
let divDodavanjeTeme = document.querySelector(".DodavanjeTeme");
let dugmeDodajTemu = divDodavanjeTeme.querySelector(".DugmeDodajTemuNovojPesmi");
let formaDodajNovuPesmu = document.querySelector(".FormaDodajNovuPesmu");
let nizTema = [];
dugmeDodajTemu.onclick = (ev)=>{

    let nazivTeme = divDodavanjeTeme.querySelector("input[name=TemaZaDodavanje]").value;
    if(nazivTeme != "")
    {
        let tema = new Tema(nazivTeme);
        nizTema.push(tema);
        divDodavanjeTeme.querySelector("input[name=TemaZaDodavanje]").value="";
    
        let div = document.createElement("div");
        div.classList.add("d-flex","flex-row","justify-content-between");
        let tekst = document.createElement("span");
        tekst.innerHTML = "#"+nazivTeme;
        div.appendChild(tekst);
        let x = document.createElement("button");
        x.classList.add("close","btn");

        let span = document.createElement("span");
        span.innerHTML = "&times";
        x.appendChild(span);

        x.onclick = (ev)=>{
            x.parentElement.parentElement.removeChild(x.parentElement);
            nizTema = nizTema.filter(el=>{
                return el.nazivTeme != x.parentElement.childNodes[0].innerHTML.split("#")[1];
            });
        }
        div.appendChild(x);
        formaDodajNovuPesmu.querySelector(".DodateTeme").appendChild(div);

    }
}

dugmeDodajNovuPesmu.onclick = (ev)=>{
    DodajNovuPesmu();
}

async function DodajNovuPesmu(){
    
    let formaDodajNovuPesmu = document.querySelector(".FormaDodajNovuPesmu");
    let nazivPesme = formaDodajNovuPesmu.querySelector("input[name=NazivPesme]").value;
    let imeAutora = formaDodajNovuPesmu.querySelector("input[name=ImeAutora]").value;
    let godinaObjavljivanja = formaDodajNovuPesmu.querySelector("input[name=GodinaObjavljivanja]").value;
    let tekstPesme = formaDodajNovuPesmu.querySelector("textarea[name=TekstPesme]").value;
    let zbirkaPesama = formaDodajNovuPesmu.querySelector("input[name=ZbirkaPesama]").value;
    let nazivEpohe = formaDodajNovuPesmu.querySelector("input[name=Epoha]").value;
    formaDodajNovuPesmu.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaDodajNovuPesmu.querySelector(".Uspesno").style.display = "none";

    if(nazivPesme == ""){
        formaDodajNovuPesmu.querySelector(".UnesiteNazivPesme").style.display = "block";
        return false;
    }

    if(imeAutora == ""){
        formaDodajNovuPesmu.querySelector(".UnesiteNazivAutora").style.display = "block";
        return false;
    }

    if(tekstPesme == ""){
        formaDodajNovuPesmu.querySelector(".UnesiteTekstPesme").style.display = "block";
        return false;
    }

    let epoha = new Epoha(nazivEpohe);
    
    await FetchDodajNovuPesmu(nazivPesme, imeAutora, godinaObjavljivanja, tekstPesme, zbirkaPesama, epoha, nizTema);
}

async function FetchDodajNovuPesmu(nazivPesme, imeAutora, godinaObjavljivanja, tekstPesme, zbirkaPesama, epoha, nizTema){

    let formaDodajNovuPesmu = document.querySelector(".FormaDodajNovuPesmu");

    let odgovor = await fetch(webapi+"Pesma/DodajNovuPesmu/" + localStorage.getItem("username"),{
        method:"POST",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        },
        body: JSON.stringify({
            
            nazivPesme : nazivPesme,
            autor : imeAutora,
            godinaObjavljivanja : godinaObjavljivanja,
            tekstPesme : tekstPesme,
            zbirkaPesama : zbirkaPesama,
            epoha : epoha,
            teme : nizTema
        })
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        formaDodajNovuPesmu.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        formaDodajNovuPesmu.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Pesma vec postoji") {
        formaDodajNovuPesmu.querySelector(".PostojiVecPesma").style.display = "block";
    }

    else if (odgovor == "Pesma uspesno dodata!") {
        formaDodajNovuPesmu.querySelector(".Uspesno").style.display = "block";
        new Pesma(null,nazivPesme, imeAutora).PesmaKaoLinkSaRednimBrojem(document.querySelector(".SvePesme"), brPesama + 1);
        brPesama++;
    }


}


let dugmeIzmeniPesmu = document.querySelector(".DugmeIzmeniPesmu");
dugmeIzmeniPesmu.onclick = (ev)=>{
    IzmeniPesmu();
}

async function IzmeniPesmu(){
    
    let formaIzmeniPesmu = document.querySelector(".FormaIzmeniPesmu");

    formaIzmeniPesmu.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaIzmeniPesmu.querySelector(".Uspesno").style.display = "none";
    
    let nazivPesme = formaIzmeniPesmu.querySelector("input[name=NazivPesme]").value;
    let imeAutora = formaIzmeniPesmu.querySelector("input[name=ImeAutora]").value;

    if (nazivPesme == "") {
        formaIzmeniPesmu.querySelector(".UnesiteNoviNazivPesme").style.display = "block";
        return false;
    }

    if (imeAutora == "") {
        formaIzmeniPesmu.querySelector(".UnesiteNazivAutora").style.display = "block";
        return false;
    }


    let novaGodinaObjavljivanja = formaIzmeniPesmu.querySelector("input[name=NovaGodinaObjavljivanja]").value.trim();
    let noviTekstPesme = formaIzmeniPesmu.querySelector("textarea[name=NoviTekstPesme]").value.trim();
    let novaZbirkaPesama = formaIzmeniPesmu.querySelector("input[name=NovaZbirkaPesama]").value.trim();
    let noviNazivEpohe = formaIzmeniPesmu.querySelector("input[name=NovaEpoha]").value.trim();

    formaIzmeniPesmu.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaIzmeniPesmu.querySelector(".Uspesno").style.display = "none";
    if(novaGodinaObjavljivanja == "@"){
        novaGodinaObjavljivanja = null;
    }

    if(noviNazivEpohe == "@"){
        noviNazivEpohe = null;
    }

    if(noviTekstPesme == "@"){
        noviTekstPesme = null;
    }

    if (novaZbirkaPesama == "@") {
        novaZbirkaPesama = null;
    }

    let epoha = new Epoha(noviNazivEpohe);
    
    await FetchIzmeniPesmu(nazivPesme, imeAutora, novaGodinaObjavljivanja, noviTekstPesme, novaZbirkaPesama, epoha);
}

async function FetchIzmeniPesmu(nazivPesme, imeAutora, novaGodinaObjavljivanja, noviTekstPesme, novaZbirkaPesama, epoha){

    let formaIzmeniPesmu = document.querySelector(".FormaIzmeniPesmu");

    let odgovor = await fetch(webapi+"Pesma/IzmeniPesmu/" +imeAutora + "/"+ nazivPesme + "/" + localStorage.getItem("username"),{
        method:"PUT",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        },
        body: JSON.stringify({
            nazivPesme: nazivPesme,
            autor: imeAutora,
            godinaObjavljivanja : novaGodinaObjavljivanja,
            tekstPesme : noviTekstPesme,
            zbirkaPesama : novaZbirkaPesama,
            epoha : epoha
        })
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        formaIzmeniPesmu.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        formaIzmeniPesmu.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
    }

    if (odgovor == "Pesma ne postoji") {
        formaIzmeniPesmu.querySelector(".NePostojiPesma").style.display = "block";
    }

    else if (odgovor == "Pesma uspesno izmenjena!") {
        formaIzmeniPesmu.querySelector(".Uspesno").style.display = "block";
    }

    

}

let dugmeObrisiPesmu = document.querySelector(".DugmeObrisiPesmu");
dugmeObrisiPesmu.onclick = (ev) => {
    ObrisiPesmu();
}

async function ObrisiPesmu() {

    let formaObrisiPesmu = document.querySelector(".FormaObrisiPesmu");
    let imeAutora = formaObrisiPesmu.querySelector("input[name=ImeAutora]").value;
    let nazivPesme = formaObrisiPesmu.querySelector("input[name=NazivPesme]").value;

    formaObrisiPesmu.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaObrisiPesmu.querySelector(".Uspesno").style.display = "none";

    if (imeAutora == "") {
        formaDodajTemuPesmi.querySelector(".UnesiteImeAutora").style.display = "block";
        return false;
    }

    if (nazivPesme == "") {
        formaDodajTemuPesmi.querySelector(".UnesiteNazivPesme").style.display = "block";
        return false;
    }

    await FetchObrisiPesmu(nazivPesme, imeAutora);
}

async function FetchObrisiPesmu(nazivPesme, imeAutora) {

    let formaObrisiPesmu = document.querySelector(".FormaObrisiPesmu");

    let odgovor = await fetch(webapi+"Pesma/ObrisiPesmu/"  + nazivPesme + "/" + imeAutora + "/" + localStorage.getItem("username"),{
        method:"DELETE",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        }
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        formaObrisiPesmu.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        formaObrisiPesmu.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
    }

    if(odgovor == "Ne postoji"){
        formaObrisiPesmu.querySelector(".PesmaNePostoji").style.display = "block";
    }

    

    if (odgovor == "Uspesno") {
        formaObrisiPesmu.querySelector(".Uspesno").style.display = "block";

        document.querySelector(".SvePesme").childNodes.forEach((e,i) => {
            if (e.querySelector("a").innerHTML == nazivPesme + " - " + imeAutora) {
                document.querySelector(".SvePesme").removeChild(e);
            }
        })
    }

}


let dugmeDodajTemuPesmi = document.querySelector(".DugmeDodajTemuPesmi");
dugmeDodajTemuPesmi.onclick = (ev) => {
    DodajTemuPesmi();
}

async function DodajTemuPesmi() {

    let formaDodajTemuPesmi = document.querySelector(".FormaDodajTemuPesmi");
    let imeAutora = formaDodajTemuPesmi.querySelector("input[name=ImeAutora]").value;
    let nazivPesme = formaDodajTemuPesmi.querySelector("input[name=NazivPesme]").value;
    let nazivTeme = formaDodajTemuPesmi.querySelector("input[name=NazivTeme]").value;
    

    formaDodajTemuPesmi.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaDodajTemuPesmi.querySelector(".Uspesno").style.display = "none";

    if (imeAutora == "") {
        formaDodajTemuPesmi.querySelector(".UnesiteImeAutora").style.display = "block";
        return false;
    }

    if (nazivPesme == "") {
        formaDodajTemuPesmi.querySelector(".UnesiteNazivPesme").style.display = "block";
        return false;
    }

    if (nazivTeme == "") {
        formaDodajTemuPesmi.querySelector(".UnesiteNazivTeme").style.display = "block";
        return false;
    }

    let tema = new Tema(nazivTeme);

    await FetchDodajTemuPesmi(imeAutora, nazivPesme, tema);

}

async function FetchDodajTemuPesmi(imeAutora, nazivPesme, tema) {

    let formaDodajTemuPesmi = document.querySelector(".FormaDodajTemuPesmi");

    let odgovor = await fetch(webapi+"EpohaTema/DodajTemuPesmi/" + imeAutora + "/" + nazivPesme + "/" + localStorage.getItem("username"),{
        method:"POST",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        },
        body: JSON.stringify({
            nazivTeme: tema.nazivTeme
        })
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        formaDodajTemuPesmi.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        formaDodajTemuPesmi.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
    }

    if(odgovor == "Pesma ne postoji!"){
        formaDodajTemuPesmi.querySelector(".PesmaNePostoji").style.display = "block";
    }

    

    if (odgovor == "Tema uspesno dodata!") {
        formaDodajTemuPesmi.querySelector(".Uspesno").style.display = "block";
    }
}

let dugmeObrisiTemuPesmi = document.querySelector(".DugmeObrisiTemuPesmi");
dugmeObrisiTemuPesmi.onclick = (ev) => {
    ObrisiTemuPesmi();
}

async function ObrisiTemuPesmi() {

    let formaObrisiTemuPesmi = document.querySelector(".FormaObrisiTemuPesmi");
    let imeAutora = formaObrisiTemuPesmi.querySelector("input[name=ImeAutora]").value;
    let nazivPesme = formaObrisiTemuPesmi.querySelector("input[name=NazivPesme]").value;
    let nazivTeme = formaObrisiTemuPesmi.querySelector("input[name=NazivTeme]").value;
    

    formaObrisiTemuPesmi.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    formaObrisiTemuPesmi.querySelector(".Uspesno").style.display = "none";

    if (imeAutora == "") {
        formaObrisiTemuPesmi.querySelector(".UnesiteImeAutora").style.display = "block";
        return false;
    }

    if (nazivPesme == "") {
        formaObrisiTemuPesmi.querySelector(".UnesiteNazivPesme").style.display = "block";
        return false;
    }

    if (nazivTeme == "") {
        formaObrisiTemuPesmi.querySelector(".UnesiteNazivTeme").style.display = "block";
        return false;
    }

    let tema = new Tema(nazivTeme);

    await FetchObrisiTemuPesmi(imeAutora, nazivPesme, tema);

}

async function FetchObrisiTemuPesmi(imeAutora, nazivPesme, tema) {

    let formaObrisiTemuPesmi = document.querySelector(".FormaObrisiTemuPesmi");

    let odgovor = await fetch(webapi+"EpohaTema/ObrisiTemuPesmi/" + imeAutora + "/" + nazivPesme + "/" + localStorage.getItem("username"),{
        method:"DELETE",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        },
        body: JSON.stringify({
            nazivTeme: tema.nazivTeme
        })
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        formaObrisiTemuPesmi.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        formaObrisiTemuPesmi.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
    }

    if(odgovor == "Pesma ne postoji!"){
        formaObrisiTemuPesmi.querySelector(".PesmaNePostoji").style.display = "block";
    }

    

    if (odgovor == "Uspesno obrisana tema pesme!") {
        formaObrisiTemuPesmi.querySelector(".Uspesno").style.display = "block";
    }
}




// Promena sifre

let dugmePromeniSifru = document.querySelector(".DugmePromeniSifru");
dugmePromeniSifru.onclick = (ev) => {
    PromeniSifru();
}

async function PromeniSifru() {

    let forma = document.querySelector(".FormaPromeniSifru");

    let sifra = forma.querySelector("input[name=Sifra]").value;
    let novaSifra = forma.querySelector("input[name=NovaSifra]").value;
    let potvrdjenaNovaSifra = forma.querySelector("input[name=PotvrdjenaSifra]").value;

    forma.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    forma.querySelector(".Uspesno").style.display = "none";

    if (sifra == "" || novaSifra == "" || potvrdjenaNovaSifra == "") {
        forma.querySelector(".PrijavljivanjePopuniteSvaPolja").style.display = "block";
        return;
    }

    await FetchPromeniSifru(sifra, novaSifra, potvrdjenaNovaSifra);

}

async function FetchPromeniSifru(sifra, novaSifra, potvrdjenaNovaSifra) {

    let forma = document.querySelector(".FormaPromeniSifru");

    let odgovor = await fetch(webapi+"User/PromenaSifre/" + localStorage.getItem("username"),{
        method:"POST",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        },
        body: JSON.stringify({
            staraSifra : sifra,
            sifra : novaSifra,
            potvrdjenaSifra : potvrdjenaNovaSifra
        })
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        forma.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        forma.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
        return;
    }

    if(odgovor == "Sifra nedovoljno duga"){
        forma.querySelector(".PrijavljivanjeNovaSifraFeedback").style.display = "block";
        return;
    }

    if (odgovor == "Sifra i potvrdjena sifra se ne poklapaju") {
        forma.querySelector(".PrijavljivanjePotvrdjenaSifraFeedback").style.display = "block";
        return;
    }

    if (odgovor == "Uneta pogresna sifra") {
        forma.querySelector(".PrijavljivanjeSifraFeedback").style.display = "block";
        return;
    }

    localStorage.setItem("token", odgovor);
    forma.querySelector(".Uspesno").style.display = "block";

}


let dugmeDodajNovogAdministratora = document.querySelector(".DugmeDodajAdministratora");
dugmeDodajNovogAdministratora.onclick = (ev) => {
    DodajNovogAdministratora();
}


async function DodajNovogAdministratora() {

    let forma = document.querySelector(".FormaDodajAdministratora");

    let korisnickoIme = forma.querySelector("input[name=KorisnickoIme]").value;
    let novaSifra = forma.querySelector("input[name=NovaSifra]").value;
    let potvrdjenaNovaSifra = forma.querySelector("input[name=PotvrdjenaSifra]").value;

    forma.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    forma.querySelector(".Uspesno").style.display = "none";

    if (korisnickoIme == "" || novaSifra == "" || potvrdjenaNovaSifra == "") {
        forma.querySelector(".PrijavljivanjePopuniteSvaPolja").style.display = "block";
        return;
    }

    await FetchDodajAdministratora(korisnickoIme, novaSifra, potvrdjenaNovaSifra);

}

async function FetchDodajAdministratora(korisnickoIme, novaSifra, potvrdjenaNovaSifra) {

    let forma = document.querySelector(".FormaDodajAdministratora");

    let odgovor = await fetch(webapi+"User/Registracija/" + localStorage.getItem("username"),{
        method:"POST",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        },
        body: JSON.stringify({
            korisnickoIme: korisnickoIme,
            sifra : novaSifra,
            potvrdjenaSifra : potvrdjenaNovaSifra
        })
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        forma.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        forma.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
        return;
    }

    if (odgovor == "Username sadrzi razmake") {
        forma.querySelector(".PrijavljivanjeKorisnickoImeFeedback").style.display = "block";
        return;
    }
    
    if (odgovor == "Username zauzet") {
        forma.querySelector(".PrijavljivanjeKorisnickoIme2Feedback").style.display = "block";
        return;
    }

    if(odgovor == "Sifra nedovoljno duga"){
        forma.querySelector(".PrijavljivanjeNovaSifraFeedback").style.display = "block";
        return;
    }

    if (odgovor == "Sifra i potvrdjena sifra se ne poklapaju") {
        forma.querySelector(".PrijavljivanjePotvrdjenaSifraFeedback").style.display = "block";
        return;
    }

    if (odgovor == "Uspesno") {
        forma.querySelector(".Uspesno").style.display = "block";
    }

}