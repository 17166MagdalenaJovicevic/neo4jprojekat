export class Pesma {

    constructor(id, nazivPesme, autor, godinaObjavljivanja, tekstPesme, zbirkaPesama, epoha, popularnost, teme) {

        this.id = id;
        this.nazivPesme = nazivPesme;
        this.autor = autor;
        this.autorRef = null;
        this.godinaObjavljivanja = godinaObjavljivanja;
        this.tekstPesme = tekstPesme;
        this.zbirkaPesama = zbirkaPesama;
        this.epoha = epoha;
        this.popularnost = popularnost;
        this.teme = [];
        if (teme != null) {
            this.teme = teme;
        }
        this.pesmeIzZbirke = [];

    }

    PrikazPesmeSaLinkom(host) {

        let kontejner = document.createElement("div");
        kontejner.classList.add("KontejnerPesmeSaLinkom","p-2");

        let card = document.createElement("div");
        card.classList.add("card-body");

        kontejner.append(card);

        let cardTitle = document.createElement("h5");
        cardTitle.classList.add("card-title");
        cardTitle.innerHTML = this.nazivPesme;

        card.append(cardTitle);

        let autor = document.createElement("div");
        autor.classList.add("fw-light");
        autor.innerHTML = this.autor;

        if (this.godinaObjavljivanja != null) {
            autor.innerHTML += " - " + this.godinaObjavljivanja;
        }

        card.append(autor);

        let tekst = document.createElement("p");
        tekst.classList.add("fw-light","fst-italic");
        tekst.innerHTML = this.tekstPesme.slice(0,60) + "...";

        card.append(tekst);


        let link = document.createElement("a");
        link.href = "prikazPesme.html?nazivPesme=" + this.nazivPesme + "&" + "imeAutora="+ this.autor;
        link.innerHTML = 'Pročitaj pesmu ' + '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-arrow-in-up-right" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M6.364 13.5a.5.5 0 0 0 .5.5H13.5a1.5 1.5 0 0 0 1.5-1.5v-10A1.5 1.5 0 0 0 13.5 1h-10A1.5 1.5 0 0 0 2 2.5v6.636a.5.5 0 1 0 1 0V2.5a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v10a.5.5 0 0 1-.5.5H6.864a.5.5 0 0 0-.5.5z"/><path fill-rule="evenodd" d="M11 5.5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793l-8.147 8.146a.5.5 0 0 0 .708.708L10 6.707V10.5a.5.5 0 0 0 1 0v-5z"/></svg>';
        link.target = "_blank";
        link.rel = "noopener noreferrer";

        card.append(link);


        host.append(kontejner);

    }

    PesmaKaoLink(host) {

        let link = document.createElement("a");
        link.innerHTML = this.nazivPesme + " - " + this.autor;
        link.href = "prikazPesme.html?nazivPesme=" + this.nazivPesme + "&" + "imeAutora="+ this.autor;
        link.target = "_blank";
        link.rel = "noopener noreferrer";
        host.append(link);

    }

    PesmaKaoLinkSaRednimBrojem(host, br) {

        let div = document.createElement("div");

        let rbr = document.createElement("span");
        rbr.innerHTML = br + ". ";
        div.append(rbr);

        let link = document.createElement("a");
        link.classList.add("text-secondary")
        link.innerHTML = this.nazivPesme + " - " + this.autor;
        link.href = "prikazPesme.html?nazivPesme=" + this.nazivPesme + "&" + "imeAutora="+ this.autor;
        link.target = "_blank";
        link.rel = "noopener noreferrer";

        div.append(link);


        host.append(div);

    }

    
    PrikazListItem(host) {

        let listItem = document.createElement("li");
        listItem.classList.add("list-group-item");

        let tip = document.createElement("span");
        tip.innerHTML = "Pesma: ";
        listItem.append(tip);

        let link = document.createElement("a");
        link.innerHTML = this.nazivPesme + " - " + this.autor;
        link.href = "prikazPesme.html?nazivPesme=" + this.nazivPesme + "&" + "imeAutora="+  this.autor;
        link.target = "_blank";
        link.rel = "noopener noreferrer";
        listItem.append(link);

        host.append(listItem);

        return listItem;

    }

    PrikazListItemTema(host) {

        let listItem = this.PrikazListItem(host);
        let tip = document.createElement("span");
        tip.innerHTML += " #" + this.teme[0].nazivTeme;
        listItem.append(tip);

    }

    PrikazListItemEpoha(host) {

        let listItem = this.PrikazListItem(host);
        let tip = document.createElement("span");
        tip.innerHTML = " Epoha: " + this.epoha.nazivEpohe;
        listItem.append(tip);

    }

}