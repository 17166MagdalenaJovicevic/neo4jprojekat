export class Tema {

    constructor(nazivTeme) {

        this.nazivTeme = nazivTeme;
        this.pesmeNaTemu = [];
        
    }

    PrikazListItem(host) {

        let listItem = document.createElement("li");
        listItem.classList.add("list-group-item");

        let tip = document.createElement("span");
        tip.innerHTML = "Tema: ";
        listItem.append(tip);

        let link = document.createElement("a");
        link.innerHTML = this.nazivTeme;
        link.href = "svePesmeNaTemu.html?nazivTeme=" + this.nazivTeme;
        link.target = "_blank";
        link.rel = "noopener noreferrer";
        listItem.append(link);

        host.append(listItem);

    }

    PrikazKaoLink(host) {

        let link = document.createElement("a");
        link.innerHTML = "#" + this.nazivTeme + " ";
        link.href = "svePesmeNaTemu.html?nazivTeme=" + this.nazivTeme;
        link.target = "_blank";
        link.rel = "noopener noreferrer";

        host.append(link);

    }

    PrikazKaoLinkSaRednimBrojem(host, br) {

        let div = document.createElement("div");

        let rbr = document.createElement("span");
        rbr.innerHTML = br + ". ";
        div.append(rbr);

        let link = document.createElement("a");
        link.innerHTML = "#" + this.nazivTeme;
        link.href = "svePesmeNaTemu.html?nazivTeme=" + this.nazivTeme;
        link.target = "_blank";
        link.rel = "noopener noreferrer";

        div.append(link);


        host.append(div);

    }

}