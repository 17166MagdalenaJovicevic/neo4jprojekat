import {Pesma} from "./pesma.js";

sessionStorage.setItem("webapi", "https://localhost:5001/");

var webapi = sessionStorage.getItem("webapi");

// 
const urlParams = new URLSearchParams(window.location.search);
var nazivEpohe = urlParams.get("nazivEpohe");

Inicijalizacija();


// 

async function Inicijalizacija() {

    document.querySelector(".Epoha").innerHTML = nazivEpohe;

    VratiSvePesmeEpohe();

}

async function VratiSvePesmeEpohe() {

    let listaPesama = await FetchVratiSvePesmeEpohe();
    if (listaPesama == null || listaPesama == false) {
        return false;
    }

    let kontejner = document.querySelector(".ListaPesama");

    listaPesama.forEach(pesma => {
        pesma.PrikazPesmeSaLinkom(kontejner);
    })

}

async function FetchVratiSvePesmeEpohe() {

    let odgovor = await fetch(webapi + "EpohaTema/VratiPesmePoEpohi/" + nazivEpohe, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesama  = [];

    odgovor.forEach(pesma => {
        listaPesama.push(new Pesma(pesma.id, pesma.nazivPesme, pesma.autor, pesma.godinaObjavljivanja, pesma.tekstPesme, null, pesma.epoha));
    })

    return listaPesama;

}