import {Pesma} from "./pesma.js";
import { Tema } from "./tema.js";
import { Pesnik } from "./pesnik.js";

sessionStorage.setItem("webapi", "https://localhost:5001/");

var webapi = sessionStorage.getItem("webapi");

const urlParams = new URLSearchParams(window.location.search);

var imePrezimePesnika = urlParams.get("imePrezime");
var pesnikObjekat;

Inicijalizacija();

async function Inicijalizacija(){

    let pesnik = await VratiPesnika();
    if (pesnik == false) {
        document.querySelector(".imePrezimePesnika").innerHTML = imePrezimePesnika + " ----- Nema podataka o pesniku";
        return false;
    }
    pesnikObjekat = pesnik;
    VratiPesmePesnika();
    VratiPesnikeIstihEpoha();
    VratiPesmeIstihEpoha();
    return pesnik;

}

async function VratiPesnika(){

    let pesnik = await FetchVratiPesnika();
    if (pesnik == false) {
        return false;
    }
    PrikazPesnika(pesnik);
    return pesnik;

}


function PrikazPesnika(pesnik){

    if (pesnik.slika != null) {
        let slikaPesnikaDOM = document.querySelector(".slikaPesnika");
        slikaPesnikaDOM.src = "data:image/*;base64," + pesnik.slika;
        slikaPesnikaDOM.style.display = "block";
    }
    

    document.querySelector(".imePrezimePesnika").innerHTML = pesnik.imePrezime;

    document.querySelector(".biografijaPesnika").innerHTML = pesnik.biografija;
    
}

async function VratiPesmePesnika(){

    let listaPesamaOdPesnika = await FetchVratiPesmePesnika();

    let kontejner1 = document.querySelector(".pesme")

    if (listaPesamaOdPesnika == null || listaPesamaOdPesnika == false)
        return false;

    listaPesamaOdPesnika.forEach((pesma, br)=>{
        pesma.PesmaKaoLinkSaRednimBrojem(kontejner1, br+1);
    })

    

}

async function FetchVratiPesnika(){

    let odgovor = await fetch(webapi + "Pesnik/VratiPesnika/" + imePrezimePesnika, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    return new Pesnik(odgovor.id, odgovor.imePrezime, odgovor.biografija, odgovor.popularnost, odgovor.slika, odgovor.epohe);
}

async function VratiPesnikeIstihEpoha(){
    
        let listaPesnikaIsteEpohe = await FetchVratiPesnikeIzIsteEpohe();

        if (listaPesnikaIsteEpohe == null || listaPesnikaIsteEpohe == false)
        return false;

        let kontejner2 = document.querySelector(".pesniciIsteEpohe");
    
        listaPesnikaIsteEpohe.forEach((pesnik)=>{
            pesnik.PrikazPesnikaSaLinkom(kontejner2);
        });
    
}

async function FetchVratiPesnikeIzIsteEpohe(){
    
    let odgovor = await fetch(webapi + "Pesnik/VratiPesnikeIstihEpoha/" + imePrezimePesnika, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesnika  = [];

    odgovor.forEach(pesnik => {
        listaPesnika.push(new Pesnik(pesnik.id, pesnik.imePrezime, pesnik.biografija, pesnik.popularnost, pesnik.slika, pesnik.epohe));
    })

    return listaPesnika;
}

async function FetchVratiPesmePesnika(){
    let odgovor = await fetch(webapi + "Pesnik/VratiPesmePesnika/" + imePrezimePesnika, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesama  = [];

    odgovor.forEach(pesma => {
        listaPesama.push(new Pesma(pesma.id, pesma.nazivPesme, pesma.autor, pesma.godinaObjavljivanja, pesma.tekstPesme, null));
    })

    return listaPesama;
}

async function VratiPesmeIstihEpoha(){

    let listaPesama = await FetchVratiPesmeIstihEpoha();

    let kontejner1 = document.querySelector(".pesmeIsteEpohe");

    if (listaPesama == null || listaPesama == false)
        return false;

    listaPesama.forEach((pesma)=>{
        pesma.PrikazPesmeSaLinkom(kontejner1);
    })

    

}

async function FetchVratiPesmeIstihEpoha(){
    let odgovor = await fetch(webapi + "Pesnik/VratiPesmeIstihEpoha/" + imePrezimePesnika, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesama  = [];

    odgovor.forEach(pesma => {
        listaPesama.push(new Pesma(pesma.id, pesma.nazivPesme, pesma.autor, pesma.godinaObjavljivanja, pesma.tekstPesme, null));
    })

    return listaPesama;
}




