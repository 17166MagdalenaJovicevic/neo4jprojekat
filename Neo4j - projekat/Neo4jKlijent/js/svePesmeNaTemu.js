import {Pesma} from "./pesma.js";

sessionStorage.setItem("webapi", "https://localhost:5001/");

var webapi = sessionStorage.getItem("webapi");

// 
const urlParams = new URLSearchParams(window.location.search);
var nazivTeme = urlParams.get("nazivTeme");

Inicijalizacija();

// 

async function Inicijalizacija() {

    document.querySelector(".Tema").innerHTML = "#" + nazivTeme;

    VratiSvePesmeNaTemu();

}

async function VratiSvePesmeNaTemu() {

    let listaPesama = await FetchVratiSvePesmeNaTemu();
    if (listaPesama == null || listaPesama == false) {
        return false;
    }

    let kontejner = document.querySelector(".ListaPesama");

    listaPesama.forEach(pesma => {
        pesma.PrikazPesmeSaLinkom(kontejner);
    })

}

async function FetchVratiSvePesmeNaTemu() {

    let odgovor = await fetch(webapi + "EpohaTema/VratiPesmePoTemi/" + nazivTeme, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesama  = [];

    odgovor.forEach(pesma => {
        listaPesama.push(new Pesma(pesma.id, pesma.nazivPesme, pesma.autor, pesma.godinaObjavljivanja, pesma.tekstPesme));
    })

    return listaPesama;

}