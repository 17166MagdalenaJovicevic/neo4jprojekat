import { Loading } from "./loading.js";
import { Pesma } from "./pesma.js";

export class Epoha {

    constructor(nazivEpohe, trajanjeEpohe) {

        this.nazivEpohe = nazivEpohe;
        this.trajanjeEpohe = trajanjeEpohe;
        this.pesmeEpohe = [];
        this.pesniciEpohe = [];

    }

    PrikazEpohe(host) {

        let dugmeEpohe = document.createElement("button");
        dugmeEpohe.classList.add("btn", "EpohaDugme");
        dugmeEpohe.innerHTML = this.nazivEpohe;
        if (this.trajanjeEpohe != null) {
            dugmeEpohe.innerHTML += " " + this.trajanjeEpohe;
        }

        dugmeEpohe.onclick = (ev) => {
            this.KlikNaEpohu();
        }

        host.append(dugmeEpohe);

    }

    PrikazEpoheKaoLinkSaRednimBrojem(host, br) {

        let div = document.createElement("div");

        let rbr = document.createElement("span");
        rbr.innerHTML = br + ". ";
        div.append(rbr);

        let link = document.createElement("a");
        link.innerHTML = this.nazivEpohe;
        if (this.trajanjeEpohe != null) {
            link.innerHTML += " " + this.trajanjeEpohe;
        }
        link.href = "svePesmePesniciEpohe.html?nazivEpohe=" + this.nazivEpohe;
        link.target = "_blank";
        link.rel = "noopener noreferrer";

        div.append(link);


        host.append(div);

    }

    PrikazListItem(host) {

        let listItem = document.createElement("li");
        listItem.classList.add("list-group-item");

        let tip = document.createElement("span");
        tip.innerHTML = "Epoha: ";
        listItem.append(tip);

        let link = document.createElement("a");
        link.innerHTML = this.nazivEpohe;
        link.href = "svePesmePesniciEpohe.html?nazivEpohe=" + this.nazivEpohe;
        link.target = "_blank";
        link.rel = "noopener noreferrer";
        listItem.append(link);

        host.append(listItem);

    }

    async KlikNaEpohu() {

        let kontejner = document.querySelector(".NajpopularnijePesmeEpohe");
        kontejner.innerHTML = "";
        new Loading().Postavi(kontejner);

        let naziv = document.createElement("h5");
        naziv.innerHTML = this.nazivEpohe;
        kontejner.append(naziv);

        let listaPesama = await this.FetchVratiNajpopularnijePesmeEpohe();
        if (listaPesama == null || listaPesama == false) {
            return false;
        }

        new Loading().Skloni(kontejner);

        listaPesama.forEach(pesma => {
            pesma.PesmaKaoLink(kontejner);
        })

        let link = document.createElement("a");
        link.classList.add("badge","badge-primary","m-2");
        link.innerHTML = "Pogledaj sve";
        link.href = "svePesmePesniciEpohe.html?nazivEpohe=" + this.nazivEpohe;
        link.target = "_blank";
        link.rel = "noopener noreferrer";


        kontejner.append(link);

    }


    async FetchVratiNajpopularnijePesmeEpohe() {

        let webapi = sessionStorage.getItem("webapi");

        let odgovor = await fetch(webapi + "EpohaTema/VratiPesmePoEpohiPopularnosti/" + this.nazivEpohe, {
            method: 'GET',
            headers: {
                "Content-Type" : "application/json"
            }
        }).catch(reason =>  { 
            return false; 
        });
    
        if (odgovor == false) {
    
            return false;
    
        }
    
        odgovor = await odgovor.json().catch(reason => {
    
            return false;
    
        });
    
        if (odgovor == false) {
    
            return false;
    
        }
    
        let listaPesama  = [];
    
        odgovor.forEach(pesma => {
            listaPesama.push(new Pesma(pesma.id, pesma.nazivPesme, pesma.autor, pesma.godinaObjavljivanja, pesma.tekstPesme));
        })
    
        return listaPesama;

    }

}