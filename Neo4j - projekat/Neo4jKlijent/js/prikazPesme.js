import {Pesma} from "./pesma.js";
import { Tema } from "./tema.js";
import { Pesnik } from "./pesnik.js";

sessionStorage.setItem("webapi", "https://localhost:5001/");

var webapi = sessionStorage.getItem("webapi");

// 
const urlParams = new URLSearchParams(window.location.search);

var nazivPesme = urlParams.get("nazivPesme");
var imeAutora = urlParams.get("imeAutora");

var pesmaObjekat;

Inicijalizacija();

async function Inicijalizacija(){

        let pesma = await VratiPesmu();
        pesmaObjekat = pesma;
        
        VratiPesmeIzIsteZbirke(pesma);

        VratiPesmeIstogPesnika(pesma);

        VratiPesmePoTemamaPesmePopularnosti();
        VratiNajpopularnijePesmeIPesnikeEpohe();


        return pesma;

        
}

async function VratiPesmu(){

    let pesma = await FetchVratiPesmu();
    PrikazPesme(pesma);
    return pesma;

}


async function FetchVratiPesmu(){

    let odgovor = await fetch(webapi + "Pesma/VratiPesmu/" + nazivPesme + "/"+ imeAutora, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    return new Pesma(odgovor.id, odgovor.nazivPesme, odgovor.autor, odgovor.godinaObjavljivanja,odgovor.tekstPesme, odgovor.zbirkaPesama, odgovor.epoha, null, odgovor.teme);
}

function PrikazPesme(pesma){

    document.querySelector(".NazivPesme").innerHTML = pesma.nazivPesme;
    
    new Pesnik(null, pesma.autor).PrikazKaoLink(document.querySelector(".AutorPesme"));
    document.querySelector(".AutorPesme").innerHTML += ", " + pesma.godinaObjavljivanja;

    document.querySelector(".TekstPesme").innerHTML = pesma.tekstPesme;

    let tagovi = document.querySelector(".Tagovi");
    pesma.teme.forEach(t => {
        let tema = new Tema(t.nazivTeme);
        tema.PrikazKaoLink(tagovi);
    })
    
}


async function VratiPesmeIzIsteZbirke(pesma){
    
    if (pesma.zbirkaPesama != null && pesma.zbirkaPesama != "") {

        let nazivZbirke = document.querySelector(".NaslovZbirkePesama");
        nazivZbirke.style.display = "block";

        nazivZbirke = document.querySelector(".NazivZbirkePesama");
        nazivZbirke.innerHTML = pesma.zbirkaPesama;

        let listaPesama = await FetchVratiPesmeIzIsteZbirke(pesma);

        let kontejner = document.querySelector(".PesmeIzIsteZbirke");

        if (listaPesama == false) {
            return;
        }
    
        listaPesama.forEach((pesma,br)=>{
            pesma.PesmaKaoLinkSaRednimBrojem(kontejner,br+1);
        });

    }

}

async function FetchVratiPesmeIzIsteZbirke(pesma){
    
    let odgovor = await fetch(webapi + "Pesma/VratiPesmeIzIsteZbirke/" + pesma.zbirkaPesama + "/" + pesma.nazivPesme + "/" + pesma.autor, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesama  = [];

    odgovor.forEach(pesma => {
        listaPesama.push(new Pesma(pesma.id, pesma.nazivPesme, pesma.autor, pesma.godinaObjavljivanja, pesma.tekstPesme, null, pesma.epoha));
    })

    return listaPesama;
}

async function VratiPesmeIstogPesnika(){

    let autor = document.querySelector(".AutorPopularnePesme");
    autor.innerHTML = imeAutora;

    let listaPesama = await FetchVratiPesmeIstogPesnika();

    let kontejner = document.querySelector(".PesmeIstogPesnika");

    if (listaPesama == null || listaPesama == false)
        return false;
    
        listaPesama.forEach(pesma=>{
            pesma.PrikazPesmeSaLinkom(kontejner);
        });

}

async function FetchVratiPesmeIstogPesnika(){
    let odgovor = await fetch(webapi + "Pesma/VratiPesmeIstogPesnika/" + nazivPesme + "/" + imeAutora, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesama  = [];

    odgovor.forEach(pesma => {
        listaPesama.push(new Pesma(pesma.id, pesma.nazivPesme, pesma.autor, pesma.godinaObjavljivanja, pesma.tekstPesme, null, pesma.epoha));
    })

    return listaPesama;
}


async function VratiPesmePoTemamaPesmePopularnosti(){

    let listaPesama = await FetchVratiPesmePoTemamaPesmePopularnosti();

    if (listaPesama == null || listaPesama == false) {
        return false;
    }

    let kontejner = document.querySelector(".PesmeIstihTema");
    
        listaPesama.forEach(pesma=>{
            pesma.PrikazPesmeSaLinkom(kontejner);
        })

}

async function FetchVratiPesmePoTemamaPesmePopularnosti(){
    let odgovor = await fetch(webapi + "Pesma/VratiPesmePoTemamaPesmePopularnosti/" + nazivPesme + "/" + imeAutora, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesama  = [];

    odgovor.forEach(pesma => {
        listaPesama.push(new Pesma(pesma.id, pesma.nazivPesme, pesma.autor, pesma.godinaObjavljivanja, pesma.tekstPesme));
    })

    return listaPesama;
}

async function VratiNajpopularnijePesmeIPesnikeEpohe() {

    if (pesmaObjekat.epoha != null) {

        let epohaNaziv = document.querySelector(".NazivEpohe");
        epohaNaziv.innerHTML = pesmaObjekat.epoha.nazivEpohe;

        let listaPesama = await FetchVratiNajpopularnijePesmeEpohe();

        let kontejner = document.querySelector(".PesmeIsteEpohe");
    
        listaPesama.forEach(pesma=>{
            pesma.PrikazPesmeSaLinkom(kontejner);
        })

        let listaPesnika = await FetchVratiNajpopularnijePesnikeEpohe();
        if (listaPesnika == null || listaPesnika == false) 
        {
            return false;
        }

        let kontejnerPesnika = document.querySelector(".PesniciIsteEpohe");

        listaPesnika.forEach(pesnik => {
            pesnik.PrikazPesnikaSaLinkom(kontejnerPesnika);
        })

    }    

}


async function FetchVratiNajpopularnijePesmeEpohe() {

    let webapi = sessionStorage.getItem("webapi");

    let odgovor = await fetch(webapi + "EpohaTema/VratiPesmePoEpohiPopularnosti/" + pesmaObjekat.epoha.nazivEpohe, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesama  = [];

    odgovor.forEach(pesma => {
        listaPesama.push(new Pesma(pesma.id, pesma.nazivPesme, pesma.autor, pesma.godinaObjavljivanja, pesma.tekstPesme));
    })

    return listaPesama;

}



async function FetchVratiNajpopularnijePesnikeEpohe() {

    let odgovor = await fetch(webapi + "EpohaTema/VratiPesnikePoEpohiPopularnosti/" + pesmaObjekat.epoha.nazivEpohe, {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json"
        }
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaPesnika  = [];

    odgovor.forEach(pesnik => {
        listaPesnika.push(new Pesnik(pesnik.id, pesnik.imePrezime, pesnik.biografija, pesnik.popularnost, pesnik.slika));
    })

    return listaPesnika;

}

